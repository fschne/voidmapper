#!/bin/sh

sed '/__content_script__/{
    s/__content_script__//g
    r src/voidmapper.js
}' voidmapper.template.svg > voidmapper.svg

sed -i '/__content_style__/{
    s/__content_style__//g
    r src/style.css
}' voidmapper.svg

sed -i '/__content_defs__/{
    s/__content_defs__//g
    r src/defs.xml
}' voidmapper.svg

TIMESTAMP=`date +"%Y%m%d-%H%M%S"`

sed -i "s/__content_update_timestamp_/${TIMESTAMP}/g" voidmapper.svg

cp -f voidmapper.svg ../web/www/voidmapper/voidmapper.svg

pandoc README.md -f markdown -t html -c https://www.f-space.de/css/markdown_default.css -s > ../web/www/voidmapper/README.html
