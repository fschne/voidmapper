/**
 * voidmapper.js
 * (c) 2019 Friedemann Schneider
 * friedemann@f-space.de
 *
 * Voidmapper is a fork of Gridmapper (https://alexschroeder.ch/wiki/Gridmapper).
 *
 * The original author (Alex Schroeder) has dedicated Gridmapper to
 * the public domain by waiving all of his rights to the work
 * worldwide under copyright law, including all related and
 * neighboring rights, to the extent allowed by law.
 *
 * More information: http://creativecommons.org/publicdomain/zero/1.0/
 */

/**
 * These two constants are used when creating new SVG elements and the
 * href attribute linking them to each other.
 */

var svgNs = "http://www.w3.org/2000/svg";
var xlinkNs = "http://www.w3.org/1999/xlink";
var inkscapeNs = "http://www.inkscape.org/namespaces/inkscape";
var xhtmlNs = "http://www.w3.org/1999/xhtml";

var DOMAIN = "www.f-space.de";
var URL = "/php/voidmapper.php";
var wsUrl = "wss://campaignwiki.org/gridmapper-server";

/**
 * Definition of all available backgrounds (cylcing with 'Z')
 */
var BACKGROUNDS = {
  lightgray: {
    class: "lightgray",
    code: "",
    message: "Default background",
    next: "black"
  },
  black: {
    class: "black",
    code: "Z",
    message: "Black background",
    next: "white"
  },
  white: {
    class: "white",
    code: "ZZ",
    message: "White background",
    next: "grid"
  },
  grid: {
    class: "grid",
    code: "ZZZ",
    message: "White background and a visible grid",
    next: "blue"
  },
  blue: {
    class: "blue",
    code: "ZZZZ",
    message: "Blue everything",
    next: "lightgray"
  }
};

/**
 * Stores everything about the map. The element members point to the
 * SVG elements where we'll be adding stuff. We need multiple elements
 * because that's how we make sure that they are painted in the
 * correct order -- a sort of primitive z-axis. The ui Element is a
 * transparent overlay where we'll register the mouse event handlers.
 *
 * If you want to change the size of the map, simply change width and
 * height. 30x30 was the template for the first One Page Dungeon
 * Contest. The tileWidth is the size of one square, in pixels. You
 * can change that as well.
 */
var Map = {
  width: 30, // how many columns
  height: 30, // how many rows
  tileWidth: 20, // tile size in pixels (change pattern #grid, too!)
  ui: null, // the SVG element with the event handlers
  pointer: null, // the SVG element for the current position
  region: null, // the SVG element to mark the region currently selected
  levelsElement: null, // the SVG element for levels
  levels: [], // data array of level data (Tiles)
  level: 0, // the current level
  data: null, // the current level data (Tiles)
  arcsElement: null, // the SVG element for the arcs of the current level
  floorElement: null, // the SVG element for the floor of the current level
  wallsElement: null, // the SVG element for the walls of the current level
  labelsElement: null, // the SVG element for the labels of the current level
  background: BACKGROUNDS.lightgray, // default background
  showOtherLevels: true, // toggle hide/show other levels '#'
  websocket: null, // the websocket used to host or listen
  leaving: false, // used to supress multiple messages

  /**
   * Initializes the datastructures if necessary. We only need to run
   * this at the very beginning. This does not reset the map in case
   * we're loading an existing file.
   *
   * Mouse and touch events: When we move the mouse (mousemove,
   * touchmove), we call Pen.update to move the pointer. When we click
   * the mouse (mousedown, touchstart), we call penDown. This changes
   * what happens when we move the mouse. Moving the mouse now calls
   * drawTo. When we end our move, we change back: Moving the mouse
   * now calls Pen.update again.
   *
   * When we run the demo, all four event handlers are set to null.
   * When the demo ends, we listen for mousedown and touchstart again.
   */
  initialize: function () {
    Map.pointer = document.getElementById("pointer");
    Map.region = document.getElementById("region").firstElementChild;
    Map.levelsElement = document.getElementById("levels");
    // http://javascript.info/tutorial/mouse-events#drag-n-drop
    Map.ui = document.getElementById("ui"); // will be moved, too
    Map.ui.ondragstart = function () {
      return false;
    };
    Map.enable();

    // http://javascript.info/tutorial/keyboard-events
    Map.exportarea = document.getElementById("exportarea");
    Map.labelfield = document.getElementById("label");
    Map.symbolInit();
  },

  /**
   * Enable editing on the map.
   */
  enable: function () {
    Map.ui.onmousemove = Pen.update;
    Map.ui.ontouchend = Pen.update;
    Map.ui.onmousedown = penDown;
    Map.ui.ontouchstart = penDown;
    Map.levelsElement;
    document.onkeydown = keyPressed; // capture arrow keys
    document.onkeypress = keyPressed; // capture character keys
  },

  /**
   * Disable editing on the map.
   */
  disable: function () {
    Map.ui.onmousemove = null;
    Map.ui.ontouchend = null;
    Map.ui.onmousedown = null;
    Map.ui.ontouchstart = null;
    document.onkeydown = null; // capture arrow keys
    document.onkeypress = null; // capture character keys
  },

  /**
   * Reset the map. This collects the minimum we need to clean
   * everything up. We run this code before starting the demo, for
   * example. There is no undo.
   */
  reset: function () {
    Commands.reset();
    Pen.reset();
    while (Map.levelsElement.lastChild)
      Map.levelsElement.removeChild(Map.levelsElement.lastChild);
    Map.levels = [];
    Map.width = 30;
    Map.height = 30;
    Map.showOtherLevels = true;
    Map.setBackground(BACKGROUNDS.lightgray);
    createLevel(0);
    Map.setLevel(0);
    Map.showLevelAnimation();
    moveElements();
  },

  /**
   * Grows the data structures, if necessary. Animate the process.
   */
  growWithAnimation: function (to_x, to_y) {
    if (to_x >= Map.width) {
      Map.width = to_x + 1;
      moveElements();
    }
    if (to_y >= Map.height) {
      Map.height = to_y + 1;
      moveElements();
    }
  },

  /**
   * Sets the current level. Use this when interpreting code.
   */
  setLevel: function (z) {
    if (!Map.levels[z]) {
      createLevel(z);
    }
    Map.level = z;
    Map.arcsElement = document.getElementById("arcs" + z);
    Map.floorElement = document.getElementById("floor" + z);
    Map.wallsElement = document.getElementById("walls" + z);
    Map.labelsElement = document.getElementById("labels" + z);
    Map.data = Map.levels[z];
    document.getElementById("level").textContent = z < 665 ? z + 1 : "The End";
    document.getElementById("maxlevel").textContent = Map.levels.length;
    resizeSVG();
  },

  /**
   * Switch to the indicated level, slowly. This is what we call when
   * the user wants to change level. Usually to_z only differs from
   * Map.level by 1.
   */
  show: function (to_z) {
    var from_z = Map.level;
    if (to_z !== from_z) {
      if (to_z > Map.level) {
        Map.level++;
      } else {
        Map.level--;
      }
      Commands.do(
        function () {
          Map.setLevel(from_z);
          Map.showLevelAnimation();
        },
        function () {
          Map.setLevel(Map.level);
          Map.showLevelAnimation();
        }
      );
      // continue if we haven't reached the target
      if (Map.level !== to_z) {
        Map.show(to_z);
      }
    }
  },

  /**
   * Switch to the indicated level, quickly. This is what we call when
   * setting the level from code and when jumping back to 0. But
   * here's the special case: when doing and undoing this, we want to
   * skip the animation. Then call Map.showLevelAnimation, once.
   */
  jump: function (to_z) {
    var from_z = Map.level;
    if (to_z !== from_z) {
      // push without doing
      Commands.push(
        function () {
          Map.setLevel(from_z);
          Map.showLevelAnimation();
        },
        function () {
          Map.setLevel(to_z);
          Map.showLevelAnimation();
        }
      );
      // just do this
      Map.setLevel(to_z);
    }
  },

  /**
   * Shows all the levels as appropriate, with animation.
   */
  showLevelsTs: null,
  showLevelAnimation: function () {
    // animate it, smoothing it out
    var now = Date.now();
    var duration = 500;
    if (Map.showLevelsTs !== null && Map.showLevelsTs > now) {
      setTimeout(Map.showLevelAnimation, Map.showLevelsTs - now);
    } else {
      Map.showLevelsTs = now - -duration;
      for (
        var z = 0, map = null;
        (map = document.getElementById("level" + z));
        z++
      ) {
        // Show upper and lower levels with a transparency effect
        // Level-1: 0.1
        // Level=0: 0
        // Level+1: 0.2
        // Level+2: 0.1
        if (!Map.showOtherLevels) {
          if (z === Map.level) {
            fadeElementTo(map, 1, linear, duration);
          } else {
            fadeElementTo(map, 0, linear, duration);
          }
        } else {
          if (z === Map.level + 2) {
            fadeElementTo(map, 0.1, linear, duration);
          } else if (z === Map.level + 1) {
            fadeElementTo(map, 0.2, linear, duration);
          } else if (z === Map.level) {
            fadeElementTo(map, 1, linear, duration);
          } else if (z === Map.level - 1) {
            fadeElementTo(map, 0.1, linear, duration);
          } else {
            var opacity = map.hasAttribute("opacity")
              ? Number(map.getAttribute("opacity"))
              : 1;
            if (opacity !== 0) {
              fadeElementTo(map, 0, linear, duration);
            }
          }
        }
      }
    }
  },

  setBackground: function (background) {
    Map.background = background;
    message(background.message);
    document.firstChild.setAttribute("class", background.class);
  },

  setHelp: function (color) {
    // default is ivory #fffff0
    var bg = document.getElementById("help").firstElementChild;
    elementTo(bg, "fill", color, linear, 500);
  },

  /**
   * Lists the replacement for a particular tile type.
   * This mapping must match the functionality of Map.code.
   */
  variants: {
    stair: "stair-small",
    "stair-small": "stair-spiral",
    "stair-spiral": "stair-big-spiral",
    "stair-big-spiral": "stair-big",
    "stair-big": "stair",
    door: "secret",
    secret: "concealed",
    concealed: "gate",
    gate: "archway",
    archway: "door",
    diagonal: "walled-diagonal",
    "walled-diagonal": "diagonal-wall",
    "diagonal-wall": "diagonal-line",
    "diagonal-line": "diagonal",
    arc: "arc2",
    arc2: "arc3",
    arc3: "arc",
    // fschne: Arcs with outer wall
    arcw: "arcw2",
    arcw2: "arcw3",
    arcw3: "arcw",
    trap: "pit",
    pit: "trap-door-ceiling",
    "trap-door-ceiling": "trap-door-floor",
    "trap-door-floor": "trap-door-secret",
    "trap-door-secret": "trap",
    statue: "well",
    well: "fountain",
    fountain: "statue",
    pillar: "altar",
    altar: "dais",
    dais: "dais-round",
    "dais-round": "rubble",
    rubble: "pillar",
    wall: "curtain",
    curtain: "portcullis",
    portcullis: "window",
    window: "gap",
    gap: "corner-pillar",
    "corner-pillar": "wall",
    chest: "bed",
    bed: "table",
    table: "chair",
    chair: "screen",
    screen: "chest",
    empty: "void",
    void: "water",
    water: "empty",
  },

  symbols: null,

  symbolInit: function () {
    // fschne: Support more colors
    // fschne: Arcs with outer wall
    var map = {
      door: "d",
      wall: "w",
      stair: "s",
      chest: "c",
      trap: "t",
      empty: "f",
      pillar: "p",
      statue: "b",
      diagonal: "n",
      arc: "a",
      arcw: "q",
      rock1: "g",
      rockd: "g",
      rock2a: "g",
      rock2b: "g",
      rock3: "g",
      rock4: "g",
      red: "R",
      green: "G",
      blue: "B",
      colorQ: "Q",
      colorE: "E",
      colorA: "A",
      colorS: "S",
      colorD: "D",
      colorF: "F",
      colorN: "N"
    };
    var keys = Object.keys(map);
    for (var i = 0; i < keys.length; i++) {
      var first = keys[i];
      var sym = map[first];
      for (
        var next = Map.variants[first];
        next !== undefined && next !== first;
        next = Map.variants[next]
      ) {
        sym += "v";
        map[next] = sym;
      }
    }
    Map.symbols = map;
  },

  /**
   * Produces the code necessary to reproduce the entire map. The
   * characters must be handled by interpretMap.
   */
  code: function () {
    var source = "";
    for (var y = 0; y < Map.height; y++) {
      for (var x = 0; x < Map.width; x++) {
        source += Map.codeFor(x, y);
      }
      source += "\n";
    }
    source = source.replace(/ +$/gm, "");
    source = source.replace(/\n+$/, "");
    source = source.replace(/^(\n*)( *)/, function (match, ys, xs) {
      if (match.length > 5) {
        return "(" + xs.length + "," + ys.length + ")";
      } else {
        return match;
      }
    });
    return source;
  },

  /**
   * Produces the code necessary to reproduce the map. The characters
   * must be handled by interpretMap.
   */
  codeFor: function (x, y) {
    var source = "";
    if (Map.data.has(x, y)) {
      var walls = Map.data.get(x, y).walls;
      if (walls) {
        walls = walls.sort(function (a, b) {
          return (
            parseInt(a.getAttribute("rotate")) -
            parseInt(b.getAttribute("rotate"))
          );
        });
        for (var i = 0; i < walls.length; i++) {
          var tile = walls[i];
          var type = Map.symbols[tile.type];
          if (type) {
            source +=
              type[0].repeat(1 + tile.getAttribute("rotate") / 90 - i) +
              type.substr(1);
          }
          if (
            tile.hasAttribute("class") &&
            tile.getAttribute("class") !== "white"
          ) {
            source += Map.symbols[tile.getAttribute("class")] + ".";
          } else if (i < walls.length - 1) {
            source += ".";
          }
        }
      }
      // label must precede floor tiles which advance the position
      var label = Map.data.get(x, y).label;
      if (label !== null) {
        if (label.textContent.search(/^[0-9]*$/) === 0) {
          source += label.textContent;
        } else if (label.getAttribute("class") === "left") {
          source += "'" + label.textContent + "'";
        } else {
          source += '"' + label.textContent + '"';
        }
      }
      var tile = Map.data.get(x, y).arcs;
      if (tile !== null) {
        if (tile.hasAttribute("rotate")) {
          var rotate = tile.getAttribute("rotate") / 90;
          var type = Map.symbols[tile.type];
          source += type[0].repeat(1 + rotate) + type.substr(1);
        } else {
          source += Map.symbols[tile.type];
        }
        // fschne: Support more colors
        if (
          tile.hasAttribute("class") &&
          tile.getAttribute("class") !== "white"
        ) {
          // append RGB and advance
          source += Map.symbols[tile.getAttribute("class")];
          if (tile.type == "empty") source += " ";
        }
      }
      tile = Map.data.get(x, y).floor;
      if (tile !== null) {
        if (tile.hasAttribute("rotate") && !tile.type.startsWith("rock")) {
          var rotate = tile.getAttribute("rotate") / 90;
          var type = Map.symbols[tile.type];
          source += type[0].repeat(1 + rotate) + type.substr(1);
        } else {
          source += Map.symbols[tile.type];
        }
        if (
          tile.hasAttribute("class") &&
          tile.getAttribute("class") !== "white"
        ) {
          // append RGB and advance
          source += Map.symbols[tile.getAttribute("class")];
          if (tile.type == "empty" || tile.type.startsWith("rock"))
            source += " ";
        }
        // the only floor tile that auto-advances is the empty floor
        if (tile.type !== "empty" && !tile.type.startsWith("rock"))
          source += " ";
      } else {
        source += " ";
      }
    } else {
      source += " ";
    }
    return source;
  },

  getPwd: function () {
    return document.getElementById("pwd").value;
  },

  getName: function () {
    return document.getElementById("name").value;
  },

  setName: function (name) {
    document.getElementById("name").value = name;
  }
};

/**
 * Provides a matrix of Tiles. If you use the get method on
 * coordinates where no Tile exists, a new one will be created.
 */
function Tiles() {
  this.data = [];
}

Tiles.prototype.get = function (x, y) {
  if (!this.data[x]) {
    this.data[x] = [];
  }
  if (!this.data[x][y]) {
    this.data[x][y] = new Tile();
  }
  return this.data[x][y];
};

Tiles.prototype.has = function (x, y) {
  return this.data[x] && this.data[x][y];
};

/**
 * Stores everything about a single square and its surrounding walls.
 */
function Tile() {
  this.arcs = null;
  this.floor = null;
  this.walls = [];
  this.label = null;
}

/**
 * Holds information about the mouse. We're trying to keep as much of
 * the coordinate messing to ourselves, here. Remember: there is an
 * empty column to the left and an empty row at the top.
 */
var Pen = {
  x: 0,
  y: 0,
  lastMove: null, // when was the pen last moved
  lastWall: null, // when was the pen last used to draw a wall or door
  type: null, // type of tile we last used
  dir: null, // last movement

  pen: 0, // the pen we switched to when collaborating
  pens: [], // other pens for collaboration with others (0 is ours)

  /**
   * Determines the grid position based on a mouse event and returns
   * it. Remember that Wall Mode has one coordinate + 0.5. Also
   * consider that in Wall mode, we need to find a point where the
   * axis changes, "jump a corner".
   */
  position: function (evt) {
    var x = evt.pageX / Map.tileWidth - 1; // empty column on the left
    var y = evt.pageY / Map.tileWidth - 1; // empty row at the top
    if (Pen.type === "wallMode") {
      var dx = x % 1;
      var dy = y % 1;
      if (Pen.x % 1 !== 0) {
        // on a vertical wall
        if (dx > 0.3 && dx < 0.7 && (dy > 0.8 || dy < 0.2)) {
          return { x: Math.floor(x + 0.5), y: Math.floor(y) + 0.5 };
        }
        // no jumping
        return { x: Math.floor(x + 0.5) - 0.5, y: Math.floor(y) };
      }
      // on a horizontal wall
      if ((dx > 0.8 || dx < 0.2) && dy > 0.3 && dy < 0.7) {
        // jump to a vertical wall
        return { x: Math.floor(x) + 0.5, y: Math.floor(y + 0.5) };
      }
      // no jumping
      return { x: Math.floor(x), y: Math.floor(y + 0.5) - 0.5 };
    }
    // in a square
    return { x: Math.floor(x), y: Math.floor(y) };
  },

  /**
   * Moves the pointer to the current Pen position. If passed a
   * non-null mouse event, determine the new position, first. We call
   * this onmousemove even if we're not drawing because of possible
   * keypress events (which don't come with a position). Exception:
   * When pressing the Alt key while moving the mouse, don't update
   * our position.
   */
  update: function (evt) {
    if (evt) {
      evt.preventDefault(); /* for touch events: no scrolling */
      if (evt.altKey) return; // Alt key: don't move the pointer
      if (evt.shiftKey) regionOn();
      // Shift key: select region
      else regionOff();
      var pos = Pen.position(evt);
      if (Pen.x !== pos.x || Pen.y !== pos.y) {
        Pen.lastMove = Date.now();
        Pen.lastWall = null;
        Pen.x = pos.x;
        Pen.y = pos.y;
        record("(" + Pen.x + "," + Pen.y + ")");
      }
    }
    Map.pointer.setAttribute("x", (Pen.x + 1) * Map.tileWidth);
    Map.pointer.setAttribute("y", (Pen.y + 1) * Map.tileWidth);
    if (Map.region.getAttribute("opacity") > 0) {
      var startX = Map.region.getAttribute("startX");
      var startY = Map.region.getAttribute("startY");
      Map.region.setAttribute(
        "x",
        (Math.min(Pen.x, startX) + 1) * Map.tileWidth
      );
      Map.region.setAttribute(
        "y",
        (Math.min(Pen.y, startY) + 1) * Map.tileWidth
      );
      Map.region.setAttribute(
        "width",
        (1 + Math.abs(Pen.x - startX)) * Map.tileWidth
      );
      Map.region.setAttribute(
        "height",
        (1 + Math.abs(Pen.y - startY)) * Map.tileWidth
      );
    }
  },

  /**
   * Moves the Pen one step closer to the target position. Returns
   * true if the Pen moved, returns false if the Pen is already at
   * the target. As Wall Mode means that one of the coordinates is
   * off by half, this needs special consideration.
   */
  moveTo: function (target) {
    // jump a corner, if necessary
    if (
      Pen.type === "wallMode" &&
      (Math.abs(Pen.x - target.x) == 0.5 || Math.abs(Pen.y - target.y) == 0.5)
    ) {
      Pen.x += target.x > Pen.x ? 0.5 : -0.5;
      Pen.y += target.y > Pen.y ? 0.5 : -0.5;
      return true;
    } else if (Pen.x !== target.x) {
      Pen.x += target.x > Pen.x ? 1 : -1;
      return true;
    } else if (Pen.y !== target.y) {
      Pen.y += target.y > Pen.y ? 1 : -1;
      return true;
    }
    return false;
  },

  /**
   * Resets the pen coordinates so that a file can be loaded safely.
   */
  reset: function () {
    Pen.x = 0;
    Pen.y = 0;
    Pen.lastWall = null;
    Pen.lastMove = null;
    Pen.type = null;
    Pen.dir = null;
  },

  /**
   * Save the current pen to its position (the first pen has the
   * number 0) and set the current pen to the saved pen in position n.
   * Remember position n. In order to switch back to the original pen,
   * call Pen.switchTo(0).
   *
   * Currently old pens aren't reaped.
   */
  switchTo: function (n) {
    Pen.pens[Pen.pen] = [
      Pen.x,
      Pen.y,
      Pen.lastMove,
      Pen.lastWall,
      Pen.type,
      Pen.dir
    ];
    if (Pen.pens[n]) {
      Pen.x = Pen.pens[n][0];
      Pen.y = Pen.pens[n][1];
      Pen.lastMove = Pen.pens[n][2];
      Pen.lastWall = Pen.pens[n][3];
      Pen.type = Pen.pens[n][4];
      Pen.dir = Pen.pens[n][5];
    } else {
      Pen.reset();
      Pen.addPointer(n);
    }
    Pen.pen = n;
    if (n > 0) {
      Map.pointer = document.getElementById("pointer" + n); // the other pen
    } else {
      Map.pointer = document.getElementById("pointer"); // my pen
    }
  },

  /**
   * Remove the pen again, if it was inactive for 10s.
   */
  addPointer: function (n) {
    var cursor = document.getElementById("cursor"); // invisible def
    pointer = document.createElementNS(svgNs, "use");
    pointer.setAttribute("id", "pointer" + n);
    pointer.setAttributeNS(xlinkNs, "href", "#others");
    pointer.setAttribute("fill", "black");
    var p = document.getElementById("pointer"); // visible element
    p.parentNode.insertBefore(pointer, p.nextSibling); // after
    setTimeout(function () {
      Pen.checkPointer(n, pointer);
    }, 10000); // 10s
  },

  /**
   * Remove the pen again, if it was inactive for 10s.
   */
  checkPointer: function (n, pointer) {
    if (Pen.pens[n]) {
      if (Date.now() - Pen.pens[n][2] < 10000) {
        // Pen.lastMove
        setTimeout(function () {
          Pen.checkPointer(n, pointer);
        }, 10000); // 10s
      } else {
        // this also happens if ts is undefined
        var ms = 1000; // 1s
        fadeElementTo(pointer, 0, linear, ms);
        setTimeout(function () {
          document.rootElement.removeChild(pointer);
        }, ms);
        Pen.pens[n] = null;
      }
    } else {
      // this should not happen because if the pen data is removed
      // when the element is removed
      console.log("checkPointer: trying to check nonexisting pen " + n);
    }
  }
};

/**
 * Maintains an undo and a redo list. Whenever you do something that
 * the user can undo, you need to call Commands.do with two
 * functions, the how-to-undo-it step and the how-to-do-it step.
 * how-to-do-it will be called once on order to do it. When the user
 * undoes something, our index is decremented and we execute the
 * appropriate undo step. When the user redoes something, our index is
 * incremented and we execute the appropriate redo step. If the user
 * has undone some steps and then pushes new commands, we discard the
 * steps we have undone.
 */
var Commands = {
  i: -1, // where are we in the undo/redo chain
  undoSteps: [],
  redoSteps: [],

  /**
   * Add two closures to the lists. The first one is how to undo a
   * change, the second one is how to do a change.
   */
  push: function (undoStep, redoStep) {
    // once we're pushing new steps, discard the undone steps
    while (this.undoSteps.length - 1 > this.i) {
      this.undoSteps.pop();
      this.redoSteps.pop();
    }
    this.undoSteps.push(undoStep);
    this.redoSteps.push(redoStep);
    this.i++;
  },

  /**
   * Add two closures to the lists. The first one is how to undo a
   * change, the second one is how to do a change. This automatically
   * does the change, too.
   */
  do: function (undoStep, redoStep) {
    Commands.push(undoStep, redoStep);
    redoStep();
  },

  undo: function () {
    if (this.i >= 0) {
      this.undoSteps[this.i--]();
    }
  },

  redo: function () {
    if (this.i < this.redoSteps.length - 1) {
      this.redoSteps[++this.i]();
    }
  },

  reset: function () {
    undoSteps = [];
    redoSteps = [];
  },

  /**
   * Make sure we can merge multiple steps on the stack into a single
   * undo/redo step.
   */
  merge: function (boundary) {
    if (this.i - boundary > 1) {
      var undoSteps = [];
      var redoSteps = [];
      for (var i = boundary; i < this.i; i++) {
        undoSteps.push(this.undoSteps.pop());
        redoSteps.unshift(this.redoSteps.pop());
      }
      this.i = boundary;
      Commands.push(
        function () {
          for (var i = 0; i < undoSteps.length; i++) {
            undoSteps[i]();
          }
        },
        function () {
          for (var i = 0; i < redoSteps.length; i++) {
            redoSteps[i]();
          }
        }
      );
    }
  }
};

/**
 * Defining repeat for Strings, for Chrome/Mac Version 40.0.2214.111
 * (64-bit) as suggested on
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/repeat
 */
if (!String.prototype.repeat) {
  String.prototype.repeat = function (count) {
    "use strict";
    if (this == null) {
      throw new TypeError("can't convert " + this + " to object");
    }
    var str = "" + this;
    count = +count;
    if (count != count) {
      count = 0;
    }
    if (count < 0) {
      throw new RangeError("repeat count must be non-negative");
    }
    if (count == Infinity) {
      throw new RangeError("repeat count must be less than infinity");
    }
    count = Math.floor(count);
    if (str.length == 0 || count == 0) {
      return "";
    }
    // Ensuring count is a 31-bit integer allows us to heavily optimize the
    // main part. But anyway, most current (august 2014) browsers can't handle
    // strings 1 << 28 chars or longer, so:
    if (str.length * count >= 1 << 28) {
      throw new RangeError(
        "repeat count must not overflow maximum string size"
      );
    }
    var rpt = "";
    for (; ;) {
      if ((count & 1) == 1) {
        rpt += str;
      }
      count >>>= 1;
      if (count == 0) {
        break;
      }
      str += str;
    }
    return rpt;
  };
}

/**
 * Defining startsWith for strings.
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith
 */
if (!String.prototype.startsWith) {
  String.prototype.startsWith = function (searchString, position) {
    position = position || 0;
    return this.substr(position, searchString.length) === searchString;
  };
}

/* The animation code is taken from these examples:
 * http://javascript.info/tutorial/animation
 */
function animate(opts) {
  var start = Date.now();
  var id = setInterval(function () {
    var timePassed = Date.now() - start;
    var progress = timePassed / opts.duration;
    if (progress > 1) progress = 1;
    var delta = opts.delta(progress);
    opts.step(delta);
    if (progress === 1) {
      clearInterval(id);
    }
  }, opts.delay || 10);
}

function linear(progress) {
  return progress;
}

function approach(progress) {
  return 1 - Math.pow(progress, 2);
}

function bounce(progress) {
  for (var a = 0, b = 1; 1; a += b, b /= 2) {
    if (progress >= (7 - 4 * a) / 11) {
      return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
    }
  }
}

function makeEaseOut(delta) {
  return function (progress) {
    return 1 - delta(1 - progress);
  };
}

var bounceEaseOut = makeEaseOut(bounce);

function resizeElement(element, toWidth, toHeight, delta, duration) {
  var fromWidth = element.getBBox().width; // assuming px
  var fromHeight = element.getBBox().height; // assuming px
  animate({
    delay: 10,
    duration: duration || 1000, // 1 sec by default
    delta: delta,
    step: function (delta) {
      element.setAttribute("width", fromWidth + (toWidth - fromWidth) * delta);
      element.setAttribute(
        "height",
        fromHeight + (toHeight - fromHeight) * delta
      );
    }
  });
}

function moveElement(element, x1, y1, x2, y2, delta, duration) {
  var re = /translate\(.*?\)/;
  var currentX = element.currentX || x1;
  var currentY = element.currentY || y1;
  var oldDx = currentX - x1;
  var oldDy = currentY - y1;
  var newDx = x2 - currentX;
  var newDy = y2 - currentY;
  element.currentX = x2;
  element.currentY = y2;
  if (newDx !== 0 || newDy !== 0)
    animate({
      delay: 10,
      duration: duration || 1000, // 1 sec by default
      delta: delta,
      step: function (delta) {
        var transform =
          "translate(" +
          (oldDx + newDx * delta) +
          "," +
          (oldDy + newDy * delta) +
          ")";
        if (!element.hasAttribute("transform"))
          element.setAttribute("transform", transform);
        else if (element.getAttribute("transform").match(re))
          element.setAttribute(
            "transform",
            element.getAttribute("transform").replace(re, transform)
          );
        else
          element.setAttribute(
            "transform",
            element.getAttribute("transform") + "," + transform
          );
      }
    });
}

function fadeElementTo(element, opacity, delta, duration) {
  var from = element.hasAttribute("opacity")
    ? Number(element.getAttribute("opacity"))
    : 1;
  animate({
    delay: 10,
    duration: duration || 1000, // 1 sec by default
    delta: delta,
    step: function (delta) {
      var value = from + (opacity - from) * delta;
      if (value > 0.99) {
        element.removeAttribute("opacity");
      } else {
        element.setAttribute("opacity", value);
      }
    }
  });
}

function elementTo(element, attribute, fill, delta, duration) {
  var str = element.getAttribute(attribute);
  var from = [211, 211, 211]; // default is light grey
  var rgb;
  if ((rgb = /^rgb\((\d+), ?(\d+), ?(\d+)\)$/i.exec(str))) {
    from = [parseInt(rgb[1]), parseInt(rgb[2]), parseInt(rgb[3])];
  } else if ((rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(str))) {
    from = [parseInt(rgb[1], 16), parseInt(rgb[2], 16), parseInt(rgb[3], 16)];
  }
  if (!isNaN(fill)) {
    fill = [fill, fill, fill];
  } else if ((rgb = /^rgb\((\d+), ?(\d+), ?(\d+)\)$/i.exec(fill))) {
    fill = [parseInt(rgb[1]), parseInt(rgb[2]), parseInt(rgb[3])];
  } else if ((rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(fill))) {
    fill = [parseInt(rgb[1], 16), parseInt(rgb[2], 16), parseInt(rgb[3], 16)];
  }
  animate({
    delay: 10,
    duration: duration || 1000, // 1 sec by default
    delta: delta,
    step: function (delta) {
      // ~~ is for Math.floor: http://rocha.la/JavaScript-bitwise-operators-in-practice
      var value = [];
      for (var i = 0; i < 3; i++) {
        value[i] = ~~(from[i] + (fill[i] - from[i]) * delta);
      }
      element.setAttribute(
        attribute,
        "rgb(" + value[0] + "," + value[1] + "," + value[2] + ")"
      );
    }
  });
}

/**
 * Updates the SVG width and height.
 * This allows browsers to scroll the image.
 */
function resizeSVG() {
  var help = document.getElementById("help").firstElementChild;
  var level = document.getElementById("level");
  document.rootElement.setAttribute(
    "width",
    (Map.width + 3) * Map.tileWidth + // map with + 3 empty columns
    Number(help.getAttribute("width")) - // width of help
    Number(help.getAttribute("x")) + // rectangle and level
    level.getBBox().width // width of level indicator
  );
  document.rootElement.setAttribute(
    "height",
    Math.max(
      (Map.height + 2) * Map.tileWidth,
      2 * Map.tileWidth + Number(help.getAttribute("height"))
    )
  );
}

/**
 * Moves the elements around if the map dimensions change.
 * Makes sure the UI element covers it all.
 * Moves and resizes the grey background.
 * This should work for a different Map.tileWidth as well!
 * We keep a timout around so that we can cancel future runs.
 */
var moveElementsTs = 0;
var moveElementsTimer = null;
function moveElements() {
  var duration = 500;
  var now = Date.now();
  if (moveElementsTs > now) {
    // we still running an animation
    if (moveElementsTimer === null) clearTimeout(moveElementsTimer); // cancel any other planned animations
    moveElementsTimer = setTimeout(moveElements, moveElementsTs - now); // plan new anim.
    return;
  } else {
    moveElementsTs = now + duration; // record for how long we'll be running
  }
  resizeSVG();
  Map.ui.setAttribute("x", Map.tileWidth);
  Map.ui.setAttribute("y", Map.tileWidth);
  Map.ui.setAttribute("width", Map.width * Map.tileWidth);
  Map.ui.setAttribute("height", Map.height * Map.tileWidth);
  for (
    var child = document.getElementById("background").firstElementChild;
    child;
    child = child.nextElementSibling
  ) {
    resizeElement(
      child,
      Map.width * Map.tileWidth,
      Map.height * Map.tileWidth,
      linear,
      duration
    );
    moveElement(
      child,
      parseInt(child.getAttribute("x")),
      parseInt(child.getAttribute("y")),
      Map.tileWidth, // an empty column to the left
      Map.tileWidth, // an empty column at the top
      linear,
      duration
    );
  }
  var help = document.getElementById("help");
  moveElement(
    help,
    help.getBBox().x,
    help.getBBox().y,
    // an empty column, the background, and another empty column to the left
    (Map.width + 2) * Map.tileWidth,
    // an empty column at the top
    Map.tileWidth,
    linear,
    duration
  );
}

/**
 * Serialize the current map as new svg document.
 */
function map_as_string(showAllLevels) {
  // create a clone but don't append it anywhere
  var clone = document.firstChild.cloneNode(false); // shallow
  var background = document.getElementById("background");
  clone.setAttribute("width", background.getBBox().width + 2 * Map.tileWidth);
  clone.setAttribute("height", background.getBBox().height + 2 * Map.tileWidth);
  clone.appendChild(document.querySelector("defs").cloneNode(true));
  clone.appendChild(document.querySelector("style").cloneNode(true));
  clone.appendChild(background.cloneNode(true));

  var levels = document.getElementById("levels").cloneNode(true);
  for (var i = 0; i < levels.childNodes.length; i++) {
    var level = levels.childNodes[i];
    var opacity = level.getAttribute("opacity");
    // fschne: Set all levels to full visibility. Hide/Show can be done later in inkscape by the user.
    if (showAllLevels) {
      level.setAttribute("opacity", 1);
    }
    //else if (opacity !== null && opacity < 1) {
    //  level.setAttribute("opacity", 0);
    //}
  }

  clone.appendChild(levels);

  // serialize
  var str =
    '<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n' +
    new XMLSerializer().serializeToString(clone);
  return str;
}

/**
 * Offer the current document for download. We store the entire
 * document in a data URI and offer it for downlod using the HTML5
 * download attribute. Chrome 40 will still load that file instead of
 * downloading it. Perhaps a namespace issue? After all, there is no
 * such download attribute for SVG or XHTML.
 */
function download_svg() {
  // base64 encoding
  var str = map_as_string(true);
  var source = btoa(unescape(encodeURIComponent(str)));
  // using image/svg+xml makes Chrome open the file instead of saving it
  downloadImmediately(
    filename() + ".svg",
    "application/octet-stream;base64",
    source
  );
}

function download_txt() {
  var source = getSource();
  downloadImmediately(filename() + ".txt", "text/plain;charset=utf-8", source);
}

function download_png(scaleFactor) {
  // base64 encoding
  var str = map_as_string(false);
  var source = btoa(unescape(encodeURIComponent(str)));

  // SVG to PNG using canvas https://gist.github.com/gustavohenke/9073132
  var canvas = document.createElementNS(xhtmlNs, "canvas");
  var background = document.getElementById("background");
  canvas.width = scaleFactor * (background.getBBox().width + 2 * Map.tileWidth); // see resizeSVG
  canvas.height =
    scaleFactor * (background.getBBox().height + 2 * Map.tileWidth);
  var ctx = canvas.getContext("2d");
  ctx.scale(scaleFactor, scaleFactor);
  var img = document.createElementNS(xhtmlNs, "img");
  img.setAttribute("src", "data:image/svg+xml;base64," + source);
  // when the image arrived in the canvas, set the link
  img.onload = function () {
    ctx.drawImage(img, 0, 0);
    var fname = filename();
    if (Map.levels.length > 1) {
      fname += "_" + (Map.level + 1) + "_" + Map.levels.length;
    }
    downloadDataUri(fname + ".png", canvas.toDataURL(""));
  };
}

function filename() {
  var name = Map.getName();
  if (name != null && name.trim() != "") {
    return name.replace(/\s+/g, "_");
  }
  return "voidmap";
}

/**
 * Download stuff
 */
function downloadImmediately(fileName, type, data) {
  // Internet Explorer: download immediately
  if (window.navigator.msSaveBlob != null) {
    window.navigator.msSaveBlob(new Blob([data]), fileName);
  } else {
    downloadDataUri(fileName, "data:" + type + "," + encodeURIComponent(data));
  }
}

/**
 * Download stuff but probably not for Internet Explorer. This is
 * useful if you already have a data URI, e.g. canvas.toDataURL.
 */
function downloadDataUri(fileName, dataUri) {
  var e = document.createElementNS(xhtmlNs, "a");
  e.setAttribute("href", dataUri);
  e.setAttribute("download", fileName);
  document.firstElementChild.appendChild(e); // Firefox requires this
  e.click();
  document.firstElementChild.removeChild(e);
}

function textSave() {
  downloadImmediately(
    filename() + ".png",
    "text/plain;charset=utf-8",
    Map.exportarea.value
  );
}

/**
 * Support for dropping a file into the textarea.
 */
function handleFileSelect(evt) {
  evt.stopPropagation();
  evt.preventDefault();
  let files = evt.dataTransfer.files; // FileList object.
  let file = files[0]; // we can only handle one file
  let reader = new FileReader();
  reader.onload = function (e) {
    Map.exportarea.value = e.target.result;
  };
  reader.readAsText(file); // UTF-8 is the default
}

/**
 * Indicate a dropzone for the textarea.
 */
function handleDragOver(evt) {
  evt.stopPropagation();
  evt.preventDefault();
  evt.dataTransfer.dropEffect = "copy"; // Explicitly show this is a copy.
}

/**
 * Stores the entire document in a link and clicks it.
 */
function link() {
  var location = window.location.href.substr(
    0,
    window.location.href.length - window.location.search.length
  );
  var url = location + "?" + encodeURIComponent(getSource());
  window.open(url, "_blank");
}

/**
 * Exports the entire document in the text area.
 */
function textExport() {
  Map.exportarea.value = getSource();
}

/**
 * Exports the code in the text area.
 */
function textImport() {
  interpretMap(Map.exportarea.value);
  Map.showLevelAnimation();
  Pen.update(null);
}

/**
 * Stores the entire document in a sequence of keystrokes with which
 * to recreate it.
 */
function getSource() {
  // do we need to export the map below
  var source = "";

  // Global settings of the map
  // Background
  source += Map.background.code;
  // Transparency
  if (!Map.showOtherLevels) {
    source += "#";
  }

  var level = Map.level;
  for (
    var z = 0, map = null;
    (map = document.getElementById("level" + z));
    z++
  ) {
    if (z > 0) source += "z";
    Map.setLevel(z);
    source += Map.code();
  }
  Map.setLevel(level); // reset
  source = source.replace(/zz+/, "z");
  source = source.replace(/^z+/, "");
  source = source.replace(/z+\w*$/, "");
  source += "(" + Pen.x + "," + Pen.y + "," + Map.level + ")\n"; // end with newline
  return source;
}

function error(str) {
  showMessage(str, "coral");
}

function message(str) {
  showMessage(str, "white");
}

function showMessage(str, backgroundColor) {
  var element = document.createElementNS(svgNs, "g");
  element.setAttribute("data-content", str);
  var rect = document.createElementNS(svgNs, "rect");
  rect.setAttribute("fill", backgroundColor);
  rect.setAttribute("stroke", "black");
  rect.setAttribute("stroke-width", "1px");
  rect.setAttribute("x", 2 * Map.tileWidth);
  rect.setAttribute("y", 2 * Map.tileWidth);
  element.appendChild(rect);
  var text = document.createElementNS(svgNs, "text");
  element.appendChild(text);
  var lines = str.split("\n");
  var len = 0;
  for (var i = 0; i < lines.length; i++) {
    var tspan = document.createElementNS(svgNs, "tspan");
    len = Math.max(len, lines[i].length);
    tspan.textContent = lines[i];
    tspan.setAttribute("x", 2.5 * Map.tileWidth);
    if (i === 0) {
      tspan.setAttribute("y", 3 * Map.tileWidth);
    } else {
      tspan.setAttribute("dy", Map.tileWidth);
    }
    text.appendChild(tspan);
  }
  rect.setAttribute("width", (2 + len / 2.5) * Map.tileWidth);
  rect.setAttribute("height", (0.5 + lines.length) * Map.tileWidth);
  document.rootElement.appendChild(element);
  var ms = 2000 + 700 * lines.length;
  fadeElementTo(element, 0, linear, ms);
  setTimeout(function () {
    document.rootElement.removeChild(element);
  }, ms);
}

/**
 * Saves the map to Campaign Wiki. This is the interactive entry
 * point. It reads title, username and summary from the exportarea
 * multiline text field.
 */
function save() {
  var name = Map.getName();
  var pwd = Map.getPwd();
  if (
    name.search(
      /^([-,.()'%&?;<> _1-9A-Za-z\u0080-\ufffd]|[-,.()'%&?;<> _0-9A-Za-z\u0080-\ufffd][-,.()'%&?;<> _0-9A-Za-z\u0080-\ufffd]+)$/
    ) !== 0
  ) {
    message(
      "Unfortunately, this name contains invalid characters. Please use [a-zA-Z _-] only."
    );
    return;
  }
  saveIt(name, pwd);
}

/**
 * Saves the current dungeon to the backend. The backend replies with 401 if the map is password protected and the giben password does not match.
 */
function saveIt(name, pwd) {
  var data = {
    name: name,
    pwd: pwd,
    map: getSource()
  };
  var content = JSON.stringify(data);
  var request = new XMLHttpRequest();
  request.addEventListener("load", function (event) {
    if (event.target.status === 200) {
      message("Saved");
    } else {
      error(event.target.responseText);
    }
  });
  request.addEventListener("error", function (event) {
    message("Unfortunately the map could not be saved.");
  });
  request.open("POST", URL);
  request.setRequestHeader("Content-Type", "application/json");
  request.send(content);
}

/**
 * Shows a drop down box where the user can pick a map from the wiki.
 */
function load() {
  message("Loading maps...");
  var request = new XMLHttpRequest();
  request.addEventListener("load", loadHtmlMenu);
  request.addEventListener("error", function (event) {
    error("Unfortunately the maps could not be loaded.");
  });
  request.open("GET", URL);
  request.send();
}

/**
 * Shows a menu of all the maps available.
 */
function loadHtmlMenu(event) {
  var maps = JSON.parse(event.target.responseText);

  maps.sort(function (a, b) {
    // localeCompare does not support locales and options arguments in Safari
    return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
  });

  var g = document.createElementNS(svgNs, "foreignObject");
  g.setAttribute("class", "menu");
  g.setAttribute("x", 2 * Map.tileWidth);
  g.setAttribute("y", 2 * Map.tileWidth);
  g.setAttribute("z", 10);
  g.setAttribute("width", Map.tileWidth * (Map.width - 2));
  g.setAttribute("height", Map.tileWidth * (Map.height - 2));
  var d = document.createElementNS(xhtmlNs, "div");
  g.appendChild(d);
  d.setAttribute("style", "height:" + Map.tileWidth * (Map.height - 3) + "px");
  for (var i = 0; i < maps.length; i++) {
    var name = maps[i].name;
    var s = document.createElementNS(xhtmlNs, "span");
    d.appendChild(s);
    var a = document.createElementNS(xhtmlNs, "a");
    s.appendChild(a);
    a.setAttribute("href", 'javascript:loadMap("' + name + '")');
    if (name.length > 20) {
      a.textContent = name.substr(0, 20) + "…";
      a.setAttribute("title", name);
    } else {
      a.textContent = name;
    }
  }

  document.rootElement.appendChild(g);
  // clicking anywhere will remove it
  var oldclick = document.onclick;
  document.onclick = function (evt) {
    document.rootElement.removeChild(g);
    document.onclick = oldclick; // probably null anyway
  };
}

/**
 * Loads map from the backend.
 */
function loadMap(name) {
  var request = new XMLHttpRequest();
  request.addEventListener("load", loadIt);
  request.addEventListener("error", function (event) {
    error("Unfortunately the map could not be loaded.");
  });

  request.open("GET", URL + "?/" + encodeURIComponent(name));
  request.send();
}

/**
 * Extract map from the wiki content.
 */
function loadIt(event) {
  var map = JSON.parse(event.target.response);

  var content = map.map;
  if (content == null || content == "") {
    message("Unfortunately this link\n" + "contains no map.");
  } else {
    message("Loading");
    Map.reset();
    record("\02");
    Map.setName(map.name);
    interpretMap(content);
    Map.showLevelAnimation();
    Pen.update(null);
  }
}

/**
 * Join a map on Campaign Wiki. It reads the map name from the
 * exportarea multiline text field.
 */

function join() {
  var map = Map.getName();
  if (map === undefined || map === "") {
    message("Please provide the name of the map!");
    return;
  }
  var pwd = Map.getPwd();

  if (Map.websocket !== null) {
    Map.websocket.close();
  }
  Map.websocket = new WebSocket(wsUrl + "/join/" + encodeURIComponent(map));
  Map.websocket.onopen = function (evt) {
    joinMap(map, pwd);
  };
  Map.websocket.onclose = function (evt) {
    leave();
  };
  Map.websocket.onmessage = function (evt) {
    if (evt.data === "\05") {
      // Requesting a new map using Ctrl-E sends the map with a Ctrl-B
      // prefix. This will get redistributed to all the clients.
      Map.websocket.send("\02" + getSource());
    } else {
      interpretRemote(evt.data); // don't use interpretMap because it's interactive
    }
  };
  Map.websocket.onerror = function (evt) {
    if (Map.websocket.readyState === WebSocket.OPEN) {
      message("Network error: " + evt.data);
    }
  };
}

function joinMap(map, pwd) {
  Map.websocket.send("\05" + pwd); // request a map reset using Ctrl-E
  message("Joined " + (pwd ? "protected" : "public") + " map " + map);
  var link = document.getElementById("join");
  link.firstChild.nodeValue = "Leave Map";
  link.setAttributeNS(xlinkNs, "href", "javascript:leave()");
}

function leave() {
  if (Map.websocket.readyState === WebSocket.OPEN) {
    Map.leaving = true;
    setTimeout(function () {
      Map.leaving = false;
    }, 100);
    Map.websocket.close(); // this will call leave again!
    message("Left map");
  } else if (!Map.leaving) {
    // if we didn't want to leave, this must be an error
    message("Cannot connect to " + wsUrl);
  }
  // this code might run twice in a row
  var link = document.getElementById("join");
  link.firstChild.nodeValue = "Join Map";
  link.setAttributeNS(xlinkNs, "href", "javascript:join()");
}

/**
 * Record all commands for hosting. This is not the same as the
 * command undo/redo history! Recorded commands are sent to the
 * websocket if were're connected.
 */
function record(key) {
  if (Map.websocket !== null && Map.websocket.readyState === WebSocket.OPEN) {
    Map.websocket.send(key);
  }
}

/**
 * Scales all the tiles in the defs element of the SVG. You can add
 * more tiles at whatever scale you want and this code will determine
 * the scale by comparing your tile's width with the tile width in use
 * by the map. The original tiles are all based on a 10x10 grid and
 * are scaled up to 20x20, for example. The easiest way to do this is
 * to use a <g> element and give it an appropriate width attribute.
 * For some user agents such as the iPad Pro, we might want to adjust
 * the tile size to get a better user experience. I wonder...
 */
function scaleTiles() {
  if (navigator.userAgent.match(/iPad/i)) {
    Map.tileWidth = 14; // instead of 20
  }
  var tiles = document.getElementsByTagName("defs")[0].childNodes;
  for (var i = 0; i < tiles.length; ++i) {
    if (
      tiles[i].nodeType === Node.ELEMENT_NODE &&
      tiles[i].hasAttribute("width")
    ) {
      var scale = Map.tileWidth / tiles[i].getAttribute("width");
      if (scale !== 1) {
        tiles[i].setAttribute(
          "transform",
          "scale(" + scale + "," + scale + ")"
        );
      }
    }
  }
}

/**
 * Creates a new tile. These tiles are use elements in the SVG
 * referring to an existing definition of the given type. An
 * appropriate id attribute is also generated.
 */
function createTile(x, y, type) {
  var tile = document.createElementNS(svgNs, "use");
  tile.setAttribute("id", type + "_" + x + "_" + y);
  tile.setAttribute("x", (x + 1) * Map.tileWidth); // empty column to the left
  tile.setAttribute("y", (y + 1) * Map.tileWidth); // empty row at the top
  tile.type = type;
  tile.setAttributeNS(xlinkNs, "href", "#" + type);
  return tile;
}

/**
 * Add s to the label at (x,y).
 */
function addToLabel(x, y, s) {
  setLabel(x, y, s, true, null); // append, middle
}

/**
 * Sets the label at (x, y) to s, or appends to the existing label.
 */
function setLabel(x, y, s, append, type) {
  var tile = Map.data.get(x, y).label;
  var text = null;
  var old = null;
  if (tile === null) {
    tile = document.createElementNS(svgNs, "text");
    tile.setAttribute("id", "label" + "_" + x + "_" + y);
    text = tile.textContent;
    if (type === null) {
      tile.setAttribute("x", (x + 1.5) * Map.tileWidth); // empty column to the left
      tile.setAttribute("y", (y + 1.7) * Map.tileWidth); // empty row at the top, + lineheight
    } else if (type === "left") {
      tile.setAttribute("class", type);
      tile.setAttribute("x", (x + 1.1) * Map.tileWidth); // empty column to the left
      tile.setAttribute("y", (y + 1.8) * Map.tileWidth); // empty row at the top, + lineheight
    }
  } else {
    old = tile.textContent;
  }
  Commands.do(
    function () {
      tile.textContent = old;
      if (old === null) {
        Map.labelsElement.removeChild(tile);
        Map.data.get(x, y).label = null;
      }
    },
    function () {
      if (text === "") {
        Map.labelsElement.appendChild(tile);
        Map.data.get(x, y).label = tile;
      }
      if (append) {
        tile.textContent += s;
      } else {
        tile.textContent = s;
      }
    }
  );
}

/**
 * Shows the input field at (x,y).
 */
function showLabelField(x, y, type) {
  fadeElementTo(Map.labelfield, 1, linear, 500);
  Map.labelfield.setAttribute("x", (x + 1) * Map.tileWidth);
  Map.labelfield.setAttribute("y", (y + 1) * Map.tileWidth);
  if (type !== null) Map.labelfield.setAttribute("class", type);
  var tile = Map.data.get(x, y).label;
  setTimeout(function () {
    // on Chrome, the text field will start with " or l from the second event
    Map.labelfield.firstElementChild.value = tile ? tile.textContent : "";
  }, 1);
  Map.labelfield.firstElementChild.focus();
}

function hideLabelField() {
  fadeElementTo(Map.labelfield, 0, linear, 500);
  Map.labelfield.firstElementChild.blur();
  // move it out of the way after waiting for it to fade
  setTimeout(function () {
    Map.labelfield.setAttribute("x", 0);
    Map.labelfield.setAttribute("y", 0);
  }, 500);
}

/**
 * Saves the text in the input field at (x, y).
 */
function saveLabel() {
  hideLabelField();
  setLabel(
    Pen.x,
    Pen.y,
    Map.labelfield.firstElementChild.value,
    false,
    Map.labelfield.getAttribute("class")
  );
}

/**
 * Remove the last character from the label at (x,y).
 * Returns true if there was text to delete.
 */
function deleteFromLabel(x, y) {
  var tile = Map.data.get(x, y).label;
  if (tile !== null) {
    // https://mathiasbynens.be/notes/javascript-unicode
    var endsWithAstralSymbol = /[\uD800-\uDBFF][\uDC00-\uDFFF]$/;
    var text = tile.textContent;
    if (text !== "") {
      Commands.do(
        function () {
          if (
            text.length === 1 ||
            (text.length === 2 && text.match(endsWithAstralSymbol))
          ) {
            Map.labelsElement.appendChild(tile);
            Map.data.get(x, y).label = tile;
          }
          tile.textContent = text;
        },
        function () {
          if (text.match(endsWithAstralSymbol))
            tile.textContent = text.substr(0, text.length - 2);
          else tile.textContent = text.substr(0, text.length - 1);
          if (tile.textContent === "") {
            Map.labelsElement.removeChild(tile);
            Map.data.get(x, y).label = null;
          }
        }
      );
      return true;
    }
  }
}

/**
 * Pick rocks matching the surrounding floor and wall tiles.
 * Return an approriately rotated tile.
 */
function getRocks(x, y) {
  var tile;
  var rockface = [true, true, true, true]; // W N E S
  // remove directions with a wall or door
  var walls = Map.data.get(x, y).walls;
  for (var i = 0; i < walls.length; i++) {
    tile = walls[i];
    var rotate = tile.getAttribute("rotate") / 90;
    rockface[rotate] = false;
  }
  // remove directions without adjacent tile
  for (var i = 0; i < 4; i++) {
    if (rockface[i] && getFloorInDirection(x, y, i) !== null) {
      rockface[i] = false;
    }
  }
  // special cases: no walls, or two opposing sides only
  if (
    [false, false, false, false].every(function (v, i) {
      return v === rockface[i];
    })
  ) {
    // don't
    return createTile(x, y, "empty");
  } else if (
    [false, true, false, true].every(function (v, i) {
      return v === rockface[i];
    })
  ) {
    tile = createTile(x, y, "rockd");
    return rotateTile(tile, x, y, 90 + 180 * (x % 2));
  } else if (
    [true, false, true, false].every(function (v, i) {
      return v === rockface[i];
    })
  ) {
    tile = createTile(x, y, "rockd");
    if (y % 2 === 1) rotateTile(tile, x, y, 180);
    return tile;
  } else {
    // check for rock<r>, biggest first
    for (var r = 4; r > 0; r--) {
      // check all four directions
      for (var i = 0; i < 4; i++) {
        var ok = true;
        // check for r rockfaces
        for (var j = 0; j < r; j++) {
          if (!rockface[(i + j) % 4]) {
            ok = false;
            break;
          }
        }
        if (ok) {
          if (document.getElementById("rock" + r)) {
            tile = createTile(x, y, "rock" + r);
          } else {
            tile = createTile(x, y, "rock" + r + (x % 2 === 1 ? "a" : "b"));
          }
          return rotateTile(tile, x, y, i * 90);
        }
      }
    }
  }
  return tile;
}

function getFloorInDirection(x, y, direction) {
  if (direction === 0) {
    return Map.data.get(x - 1, y).floor;
  } else if (direction === 1) {
    return Map.data.get(x, y - 1).floor;
  } else if (direction === 2) {
    return Map.data.get(x + 1, y).floor;
  } else if (direction === 3) {
    return Map.data.get(x, y + 1).floor;
  }
}

/**
 * Draw rocks. If we already have rocks, draw a floor instead. This is
 * very similar to the draw function except we create a special kind
 * of tile.
 */
function rock(x, y) {
  var old = Map.data.get(x, y).floor;
  if (old !== null && old.type.startsWith("rock")) {
    replaceFloor(x, y, old, null);
  } else {
    replaceFloor(x, y, old, getRocks(x, y));
  }
  checkRockyNeighbors(x, y);
}

/**
 * Check whether any of the neighbors need adjustments. This happens
 * when adding a rock tile or when removing a normal tile surrounded
 * by rock tiles. When adjusting neighbors in this way, make sure we
 * keep their background color.
 */
function checkRockyNeighbors(x, y) {
  // check neighbors for adjustment
  var undoBoundary = Commands.i - 1;
  old = Map.data.get(x - 1, y).floor;
  if (old !== null && old.type.startsWith("rock"))
    replaceColoredFloor(x - 1, y, old, getRocks(x - 1, y), true);
  old = Map.data.get(x, y - 1).floor;
  if (old !== null && old.type.startsWith("rock"))
    replaceColoredFloor(x, y - 1, old, getRocks(x, y - 1), true);
  old = Map.data.get(x + 1, y).floor;
  if (old !== null && old.type.startsWith("rock"))
    replaceColoredFloor(x + 1, y, old, getRocks(x + 1, y), true);
  old = Map.data.get(x, y + 1).floor;
  if (old !== null && old.type.startsWith("rock"))
    replaceColoredFloor(x, y + 1, old, getRocks(x, y + 1), true);
  // make sure this is a single undo/redo command
  Commands.merge(undoBoundary);
}

/**
 * Returns an unused angle for wall tiles. Provide a preferred angle.
 * Possible values are 0, 90, 180, 270.
 */
function getUnusedAngle(x, y, rotate) {
  var candidate = [
    rotate,
    (rotate + 90) % 360,
    (rotate + 180) % 360,
    (rotate + 270) % 360
  ];
  var existingWalls = [];
  var walls = Map.data.get(x, y).walls;
  for (var i = 0; i < walls.length; i++) {
    existingWalls.push(walls[i].getAttribute("rotate"));
  }
  search: for (var c = 0; c < 4; c++) {
    for (var i = 0; i < existingWalls.length; i++) {
      if (candidate[c] == existingWalls[i]) {
        continue search;
      }
    }
    return candidate[c];
  }
}

/* Drawing on the walls */

/**
 * Modifies the given tile such that it is rotated by the given
 * angle. The tile is also returned for convenience.
 */
function rotateTile(tile, x, y, angle) {
  tile.setAttribute("rotate", angle);
  if (angle > 0) {
    var cx = (x + 1.5) * Map.tileWidth; // empty column to the left
    var cy = (y + 1.5) * Map.tileWidth; // empty row at the top
    tile.setAttribute(
      "transform",
      "rotate(" + angle + "," + cx + "," + cy + ")"
    );
  } else {
    tile.removeAttribute("transform");
  }
  return tile;
}

/**
 * Places a tile of the given type on a free section of the wall
 * around the given square. Wall placement sets Pen.lastWall, because
 * walls require timing. If you're quick, the wall is rotate. If
 * you're slow, another wall is created.
 */
function wallPlacement(x, y, type) {
  var now = Date.now();
  var walls = Map.data.get(x, y).walls;
  if (
    Pen.type === type &&
    walls &&
    walls.length > 0 &&
    Pen.lastWall &&
    now - Pen.lastWall <= 2000
  ) {
    // we just placed a door: take the last one and rotate it
    var tile = walls[walls.length - 1];
    var old = tile.getAttribute("transform");
    var angle = parseInt(tile.getAttribute("rotate"));
    var rotate = getUnusedAngle(x, y, (angle + 90) % 360);
    if (rotate !== null) {
      type = tile.type; // use 'concealed' instead of 'door' for example
      Commands.do(
        function () {
          rotateTile(tile, x, y, angle);
        },
        function () {
          rotateTile(tile, x, y, rotate);
        }
      );
    }
  } else {
    var rotate = getUnusedAngle(x, y, 0);
    if (rotate !== null) {
      var tile = createTile(x, y, type);
      rotateTile(tile, x, y, rotate);
      Commands.do(
        function () {
          walls.pop();
          Map.wallsElement.removeChild(tile);
        },
        function () {
          walls.push(tile);
          Map.wallsElement.appendChild(tile);
        }
      );
    }
  }
  Pen.lastWall = now;
}

/**
 * Draws a wall tile of the given type on the current wall. The
 * current wall only makes sense in Wall Mode. In Wall Mode, one of
 * the two coordinates is +0.5. We determine the angle (0 or 90) and
 * put a tile there.
 */
function wallDraw(x, y, type) {
  var angle = 0;
  if (y % 1 === 0) {
    x = Math.ceil(x);
  } else {
    angle = 90;
    y = Math.ceil(y);
  }
  var i, old;
  var tile = createTile(x, y, type);
  var walls = Map.data.get(x, y).walls;
  rotateTile(tile, x, y, angle);
  for (i = 0; i < walls.length; i++) {
    if (parseInt(walls[i].getAttribute("rotate")) == angle) {
      old = walls[i];
      break;
    }
  }
  Commands.do(
    function () {
      Map.wallsElement.removeChild(tile);
      if (old) {
        Map.wallsElement.appendChild(old);
        walls[i] = old;
      } else {
        walls.pop();
      }
    },
    function () {
      if (old) {
        Map.wallsElement.removeChild(old);
        walls[i] = tile;
      } else {
        walls.push(tile);
      }
      Map.wallsElement.appendChild(tile);
    }
  );
}

/**
 * Toggles Wall Mode.
 */
function wallMode(x, y) {
  if (Pen.type === "wallMode") {
    wallModeOff();
  } else {
    regionOff();
    Pen.type = "wallMode";
    if (Pen.x > 0) {
      Pen.x -= 0.5;
    } else {
      Pen.x += 0.5;
    }
  }
  Pen.update(null);
}

/**
 * Places a tile of the given type in the given square. If in Wall
 * Mode, use the given wall. Otherwise, find a free section around the
 * given square.
 */
function wall(x, y, type) {
  if (Pen.type === "wallMode") {
    wallDraw(x, y, type);
  } else {
    wallPlacement(x, y, type);
    Pen.type = type;
  }
}

/**
 * Finds a wall tile for the current position of a given angle in wall
 * mode and return its index in the wall array. You're meant to call
 * this multiple times: Once for (x, y) where the angle could be 0° or
 * 90°, once for (x-1, y) where the angle could be 180° and once for
 * (x, y-1) where the angle could be 270°.
 */
function findWall(walls, angle) {
  for (var i = 0; i < walls.length; i++) {
    if (parseInt(walls[i].getAttribute("rotate")) == angle) {
      return i;
    }
  }
  return null;
}

/**
 * Removes a tile from the surrounding walls of the give tile.
 * Returns true if there was a wall.
 */
function removeWall(x, y) {
  var angle = 0;
  if (Pen.type === "wallMode") {
    if (y % 1 === 0) {
      x = Math.ceil(x);
    } else {
      angle = 90;
      y = Math.ceil(y);
    }
  }
  var walls = Map.data.get(x, y).walls;
  var i;
  if (Pen.type === "wallMode") {
    i = findWall(walls, angle);
    if (i === null && angle === 0 && x > 0) {
      walls = Map.data.get(x - 1, y).walls;
      i = findWall(walls, 180);
    }
    if (i === null && angle === 90 && y > 0) {
      walls = Map.data.get(x, y - 1).walls;
      i = findWall(y - 1, y, 270);
    }
  } else {
    i = walls.length - 1;
  }
  if (i !== null && i >= 0) {
    var old = walls[i];
    Commands.do(
      function () {
        Map.wallsElement.appendChild(old);
        walls.push(old);
      },
      function () {
        Map.wallsElement.removeChild(old);
        walls.splice(i, 1);
      }
    );
    return true;
  }
}

/**
 * Draws a tile on the floor. If the tile it already there, draws
 * floor instead. Stairs are taken care of---they are on a different
 * layer!
 */
function draw(x, y, type) {
  var old = Map.data.get(x, y).floor;
  if (old !== null && old.type === type) {
    floor(x, y);
  } else {
    replaceFloor(x, y, old, createTile(x, y, type));
  }
}

/**
 * Replaces the old tile on the floor with a new tile. The code
 * handles both the old tile and the new tile being null. If both are
 * null, nothing needs to be done. If something was done, return true.
 * This is used when hitting DEL to remove a wall, or a floor tile, or
 * an arc. This is the main function at the end of all floor drawing.
 */
function replace(data, element, x, y, old, tile, recursive) {
  if (old !== null || tile !== null) {
    Commands.do(
      function () {
        if (tile) element.removeChild(tile);
        if (old) element.appendChild(old);
        Map.data.get(x, y)[data] = old;
      },
      function () {
        if (old) element.removeChild(old);
        if (tile) element.appendChild(tile);
        Map.data.get(x, y)[data] = tile;
      }
    );
    Pen.lastWall = null;
    if (!recursive) checkRockyNeighbors(x, y);
    return true;
  }
}

function replaceFloor(x, y, old, tile) {
  return replace("floor", Map.floorElement, x, y, old, tile, false);
}

/**
 * When automatically adjusting a tile such as grottos, we do not want
 * to change their background color. In this case, we're also calling
 * replace recursively and thus we need to prevent it from processing
 * further grottos.
 */
function replaceColoredFloor(x, y, old, tile, recursive) {
  if (old.hasAttribute("class"))
    tile.setAttribute("class", old.getAttribute("class"));
  return replace("floor", Map.floorElement, x, y, old, tile, recursive);
}

function replaceArc(x, y, old, tile) {
  return replace("arcs", Map.arcsElement, x, y, old, tile, false);
}

function background(x, y, color) {
  var tile;
  if (Pen.type === "wallMode") {
    var angle = 0;
    if (y % 1 === 0) {
      x = Math.ceil(x);
    } else {
      angle = 90;
      y = Math.ceil(y);
    }
    var walls = Map.data.get(x, y).walls;
    for (var i = 0; i < walls.length; i++) {
      if (parseInt(walls[i].getAttribute("rotate")) == angle) {
        tile = walls[i];
        break;
      }
    }
  } else {
    var now = Date.now();
    var walls = Map.data.get(x, y).walls;
    if (
      walls &&
      walls.length > 0 &&
      Pen.lastWall &&
      now - Pen.lastWall <= 2000
    ) {
      tile = walls[walls.length - 1];
    } else {
      tile = Map.data.get(x, y).floor || Map.data.get(x, y).arcs;
    }
  }
  var undoBoundary = Commands.i;
  if (tile == null) {
    wallModeOff();
    floor(x, y);
    tile = Map.data.get(x, y).floor;
  }
  var old = tile.getAttribute("class");
  Commands.do(
    function () {
      tile.setAttribute("class", old);
    },
    function () {
      tile.setAttribute("class", color);
    }
  );
  // make sure this is a single undo/redo command
  Commands.merge(undoBoundary);
}

function regionOff(x, y) {
  if (Map.region.getAttribute("opacity") > 0) {
    Map.region.setAttribute("opacity", 0);
  }
}

/**
 * Turn the region on if it was off. This also marks the starting
 * position of the region. Since this is only set when the region was
 * off, you can call it any number of times after that.
 */
function regionOn(x, y) {
  if (Map.region.getAttribute("opacity") == 0) {
    wallModeOff();
    Map.region.setAttribute("x", (Pen.x + 1) * Map.tileWidth);
    Map.region.setAttribute("y", (Pen.y + 1) * Map.tileWidth);
    Map.region.setAttribute("startX", Pen.x);
    Map.region.setAttribute("startY", Pen.y);
    Map.region.setAttribute("height", Map.tileWidth);
    Map.region.setAttribute("width", Map.tileWidth);
    Map.region.setAttribute("opacity", 0.2);
  }
}

/**
 * Delete everything at every position for the rectangle specified.
 */
function rectangleDelete(x1, y1, x2, y2) {
  for (var y = y1; y <= y2; y++) {
    for (var x = x1; x <= x2; x++) {
      deleteFromLabel(x, y);
      while (removeWall(x, y));
      removeFloor(x, y);
      removeArc(x, y);
    }
  }
}

/**
 * Loop over the entire region, if it is active, and delete everything
 * at every position. FIXME: merge undo boundaries?
 */
function regionDelete() {
  if (Map.region.getAttribute("opacity") > 0) {
    var startX = Map.region.getAttribute("startX");
    var startY = Map.region.getAttribute("startY");
    rectangleDelete(
      Math.min(Pen.x, startX),
      Math.min(Pen.y, startY),
      Math.max(Pen.x, startX),
      Math.max(Pen.y, startY)
    );
  }
}

function regionCut() {
  let undoBoundary = Commands.i;
  regionCopy();
  regionDelete();
  Commands.merge(undoBoundary);
}

function regionPaste() {
  let undoBoundary = Commands.i;
  textImport();
  Commands.merge(undoBoundary);
}

/**
 * Loop over the entire region, if it is active, and create a string
 * containing all the commands necessary to recreate it, and put it
 * into the text area. If the region is inactive, enable it.
 */
function regionCopy() {
  regionOn();
  // start with a deletion of the target region
  let startX = Map.region.getAttribute("startX");
  let startY = Map.region.getAttribute("startY");
  let source =
    "X[" +
    (1 + Math.abs(Pen.x - startX)) +
    "," +
    (1 + Math.abs(Pen.y - startY)) +
    "]";
  // now generate the code for the region
  for (let y = Math.min(Pen.y, startY); y <= Math.max(Pen.y, startY); y++) {
    for (let x = Math.min(Pen.x, startX); x <= Math.max(Pen.x, startX); x++) {
      source += Map.codeFor(x, y);
    }
    if (y < Math.max(Pen.y, startY)) {
      source += "[-" + (1 + Math.abs(Pen.x - startX)) + ",1]";
    } else {
      source += "-";
    }
  }
  Map.exportarea.value = source;
}

/**
 * Returns the number of steps required to turn b into variant a. If a
 * is not a variant of b, returns undefined. From stairs to stairs is
 * 0, so this is not truthy! Use isVariantOf if you need that.
 */
function variantOf(a, b) {
  if (a === b) return 0;
  var matches = {};
  for (
    var next = b, i = 1;
    next !== undefined && Map.variants[next] !== b;
    next = Map.variants[next], i++
  ) {
    matches[next] = i;
  }
  return matches[a];
}

/**
 * Returns whether a is a variant of b.
 */
function isVariantOf(a, b) {
  if (a === b) return true;
  return variantOf(a, b) !== undefined;
}

/**
 * Replaces the last tile placed with a variant thereof.
 */
function variant(x, y) {
  var tile;
  if (Pen.type === "wallMode") {
    if (y % 1 === 0) {
      x = Math.ceil(x);
    } else {
      y = Math.ceil(y);
    }
  }
  tile = Map.data.get(x, y).floor || Map.data.get(x, y).arcs;
  if (
    Pen.type === "wallMode" ||
    isVariantOf(Pen.type, "door") ||
    isVariantOf(Pen.type, "wall") ||
    (tile !== null && Map.variants[tile.type] === undefined)
  ) {
    var walls = Map.data.get(x, y).walls;
    tile = walls[walls.length - 1];
  }
  if (tile) {
    var old = tile.type;
    var type = Map.variants[tile.type];
    if (type) {
      Commands.do(
        function () {
          tile.type = old;
          tile.setAttribute("id", old + "_" + x + "_" + y);
          tile.setAttributeNS(xlinkNs, "href", "#" + old);
        },
        function () {
          tile.type = type;
          tile.setAttribute("id", type + "_" + x + "_" + y);
          tile.setAttributeNS(xlinkNs, "href", "#" + type);
        }
      );
    }
  }
}

/**
 * Draws stairs, arcs and diagonals on the floor. 'data' is the key
 * into Tile, ie. one of 'arcs', 'floor' or 'walls'. Set Pen.lastWall
 * to null when calling rotateTile. We can't do it there because
 * rotateTile is also called by wallPlacement. We don't need to do it
 * when calling replace because we'll do it there.
 */
function rotate(data, x, y, type) {
  var old = Map.data.get(x, y)[data];
  if (
    old &&
    isVariantOf(old.type, type) !== null &&
    old.hasAttribute("rotate")
  ) {
    var angle = parseInt(old.getAttribute("rotate"));
    Commands.do(
      function () {
        rotateTile(old, x, y, angle);
      },
      function () {
        rotateTile(old, x, y, (angle + 90) % 360);
      }
    );
    Pen.lastWall = null;
  } else {
    var tile = createTile(x, y, type);
    tile.setAttribute("rotate", 0);
    // fschne: Arcs with outer wall
    if (type === "arc" || type === "arcw")
      replace(data, Map.arcsElement, x, y, old, tile);
    else replace(data, Map.floorElement, x, y, old, tile);
  }
}

/**
 * Draws or erases a floor tile.
 */
function floor(x, y) {
  var old = Map.data.get(x, y).floor;
  if (!old || old.type !== "empty") {
    replaceFloor(x, y, old, createTile(x, y, "empty"));
  } else {
    replaceFloor(x, y, old, null);
  }
}

/**
 * Erase floor tile, if any.
 */
function removeFloor(x, y) {
  var old = Map.data.get(x, y).floor;
  if (old) {
    replaceFloor(x, y, old, null);
    checkRockyNeighbors(x, y);
    return old;
  }
}

/**
 * Erase arc tile, if any.
 */
function removeArc(x, y) {
  var old = Map.data.get(x, y).arcs;
  if (old) return replaceArc(x, y, old, null);
}

/**
 * This is where we draw (or erase) floor tiles. The key point to
 * remember is that we might not get a mouse event for every
 * coordinate. That's why we need to store where we want to draw to
 * (our target) and start drawing at our current position (the pen).
 * We do this at least once in order to support clicks without mouse
 * movement. Then we start a loop: as long as the pen hasn't reached
 * the target, we change x or y by 1 and draw another tile. As we're
 * creating closures for Commands, we need to do this in a factory.
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures#Creating_closures_in_loops.3A_A_common_mistake
 */
function drawToCommand(x, y, tile, type) {
  record("(" + x + "," + y + ")f");
  var remove = function () {
    Map.floorElement.removeChild(tile);
    Map.data.get(x, y).floor = null;
    checkRockyNeighbors(x, y);
  };
  var add = function () {
    Map.data.get(x, y).floor = tile;
    Map.floorElement.appendChild(tile);
    checkRockyNeighbors(x, y);
  };
  if (type === "add") {
    Commands.do(remove, add); // add new tile
  } else {
    Commands.do(add, remove); // remove existing tile
  }
}

/**
 * Call drawToCommand based on the position data in the event.
 */
function drawTo(evt) {
  var target = Pen.position(evt);
  // draw up to the current position (at least once if we just started)
  do {
    if (!Map.data.get(Pen.x, Pen.y).floor && Pen.type === "empty") {
      drawToCommand(Pen.x, Pen.y, createTile(Pen.x, Pen.y, "empty"), "add");
    } else if (Map.data.get(Pen.x, Pen.y).floor && Pen.type === "null") {
      drawToCommand(Pen.x, Pen.y, Map.data.get(Pen.x, Pen.y).floor, "remove");
    }
  } while (Pen.moveTo(target));
  Pen.update(evt);
  if (Pen.type === "empty") Map.growWithAnimation(Pen.x, Pen.y);
}

/**
 * Sets the Pen down, ready for drawing. When setting down the pen
 * (onmousedown, penDown), we look at the current tile. If it's the
 * empty floor tile, we'll want to erase it. If there is no floor tile
 * (null), we'll want to draw an empty floor tile. We store this
 * information in the Pen type and as we keep on drawing (onmousemove,
 * drawTo), we don't change it.
 *
 * As for touch screens: If we are just touching the screen, don't
 * draw a tile. If we keep touching the screen, however, draw a tile.
 */
function penDown(evt) {
  wallModeOff();
  Pen.update(evt);
  if (Map.data.get(Pen.x, Pen.y).floor) {
    Pen.type = "null";
  } else {
    Pen.type = "empty";
  }
  // Draw a tile unless we just touched the screen. These variables
  // are used in ontouchend below.
  if (evt.type === "touchstart") {
    var ts = Date.now();
    var pos = Pen.x + "," + Pen.y;
  } else {
    drawTo(evt);
  }
  // install new handler
  Map.ui.onmousemove = drawTo;
  Map.ui.ontouchmove = drawTo;
  // revert to pen tracking on mouse up *anywhere*
  document.onmouseup = function () {
    Map.ui.onmousemove = Pen.update;
  };
  document.ontouchend = function () {
    // if this was a long touch and we haven't moved, draw a tile anyway
    if (Date.now() > ts + 300 && pos === Pen.x + "," + Pen.y) {
      drawTo(evt);
    }
    Map.ui.ontouchmove = Pen.update;
  };
}

/**
 * Puts the pointer back into a square and ends Wall Mode.
 * Call this whenever you're acting on squares.
 */
function wallModeOff() {
  Pen.x = Math.ceil(Pen.x);
  Pen.y = Math.ceil(Pen.y);
  Pen.update(null);
  Pen.type = null;
}

/**
 * Moves the pointer around. It handles both moving from square to
 * sqare as well as moving from wall to wall (where one of the
 * coordinates is off by 0.5). Moving from horizontal to vertical
 * walls and back in wall mode is timing sensitive. That's why we need
 * Pen.lastMove. We also set Pen.lastWall to null, so timing sensitive
 * wall placement also works as intended.
 */
function move(dir) {
  switch (dir) {
    case "left":
      if (Pen.x > 0) Pen.x--;
      break;
    case "right":
      Map.growWithAnimation(Pen.x + 1, Pen.y);
      Pen.x++;
      break;
    case "up":
      if (Pen.y > 0) Pen.y--;
      break;
    case "down":
      Map.growWithAnimation(Pen.x, Pen.y + 1);
      Pen.y++;
      break;
  }
  // Example: when moving down from 1 to 2 and right to 3, we want to
  // switch position on the grid and end up on 4. So we need to adjust
  // the position by (-0.5,-0.5):
  //
  //  x→
  // y  |   |
  // ↓--+---+---
  //    1   |
  //  --+-4-+---
  //    2   3
  //  --+---+---
  //    |   |
  //
  // If we moved first right and then down, we would end up in the same
  // position.
  var now = Date.now();
  if (Pen.type === "wallMode") {
    if (Pen.dir && Pen.lastMove && now - Pen.lastMove <= 500) {
      if (
        (Pen.dir == "left" && dir == "down") ||
        (Pen.dir == "down" && dir == "left")
      ) {
        Pen.x += 0.5;
        Pen.y -= 0.5;
      } else if (
        (Pen.dir == "left" && dir == "up") ||
        (Pen.dir == "up" && dir == "left")
      ) {
        Pen.x += 0.5;
        Pen.y += 0.5;
      } else if (
        (Pen.dir == "right" && dir == "down") ||
        (Pen.dir == "down" && dir == "right")
      ) {
        Pen.x -= 0.5;
        Pen.y -= 0.5;
      } else if (
        (Pen.dir == "right" && dir == "up") ||
        (Pen.dir == "up" && dir == "right")
      ) {
        Pen.x -= 0.5;
        Pen.y += 0.5;
      }
    }
  }
  Pen.update(null);
  Pen.dir = dir;
  Pen.lastMove = now; // must come after Pen.update
  Pen.lastWall = null; // reset the wall counter
}

/**
 * Extract key from KeyboardEvent Basic source from
 * http://javascript.info/tutorial/keyboard-events#processing-the-character-keypress
 */
function getChar(event) {
  if (event.which == null) {
    return String.fromCharCode(event.keyCode); // IE character keys
  } else if (event.which != 0 && event.charCode != 0) {
    return String.fromCharCode(event.which); // the rest character keys
  } else if (event.type == "keydown") {
    // special key
    if (event.key && event.key.length !== 1) return event.key; // Firefox
    if (event.keyIdentifier && event.keyIdentifier.substring(0, 2) !== "U+")
      return event.keyIdentifier; // Chrome
    if (event.keyCode === 8) return "Backspace"; // Chrome Backspace
  }
  return null; // ignore second key event
}

/**
 * Handle key presses, recording them all.
 */
function keyPressed(evt) {
  keyHandling(evt, true);
}

/**
 * Processes key press events and single character strings (in case
 * the interpreter is feeding us some). Many of the key commands need
 * a 'current' position. We determine it by looking at the Pen. It's
 * position is always tracked (onmousemove, Pen.update). When
 * interpreting code we received from a remote host, we don't want to
 * be recording, which is why we need a flag.
 */
function keyHandling(evt, recording) {
  //console.log(evt);

  // We don't handly any meta keys.
  if (evt.metaKey) return;

  // Could be a string when running interpreted code.
  var key = evt;

  // When running the demo, ignore key events from the browser.
  if (evt instanceof KeyboardEvent) {
    key = getChar(evt);
    if (key === null) return; // ignore one of onkeydown or onkeypress event
  }

  var target = evt.originalTarget || evt.srcElement; // Chrome
  var isTextarea = target != null && target == Map.exportarea;
  var isInputfield = target != null && target.tagName == "input";
  var isLabelfield = target != null && target.parentNode == Map.labelfield;

  // Ctrl+Enter on the text field
  if (isTextarea) {
    if (key === "Enter" && evt.ctrlKey) {
      textImport();
    }
    return;
  }

  // Ctrl is the same as Shift
  // upcase control keys and use them instead
  if (evt.ctrlKey) {
    if (evt.keyCode) {
      // Chrome
      key = String.fromCharCode(64 + evt.keyCode); // 3 is 67 is 'C'
    } else if (key !== "Control") {
      key = String.toUpperCase(key);
    }
  }

  // Labelfield
  if (isLabelfield) {
    if (key === "Enter") {
      var type = Map.labelfield.getAttribute("class");
      saveLabel();
      if (type === null) {
        record('"' + Map.data.get(Pen.x, Pen.y).label.textContent + '"');
      } else if (type === "left") {
        record("'" + Map.data.get(Pen.x, Pen.y).label.textContent + "'");
      }
    } else if (key === "Esc" || key === "Escape") {
      hideLabelField();
    }
    return;
  }

  // Do not handle events from password or name input.
  if (isInputfield) {
    return;
  }

  if (key === "Backspace") {
    evt.preventDefault();
  }

  var keepRegion = false;

  switch (key) {
    case "?":
      var help = document.getElementById("help");
      if (help.getAttribute("visibility") === "hidden") {
        help.setAttribute("visibility", "visible");
      } else {
        help.setAttribute("visibility", "hidden");
      }
      break;

    case "#":
      Map.showOtherLevels = !Map.showOtherLevels;
      Map.showLevelAnimation();
      break;

    case ".":
      Pen.lastWall = null;
      break;

    case "u":
      Commands.undo();
      break;

    case "r":
      Commands.redo();
      break;

    case "ArrowDown":
    case "Down":
    case "j":
    case "J":
      if (evt.altKey) {
        evt.preventDefault();
        wallModeOff();
        floor(Pen.x, Pen.y);
      }
      if (evt.shiftKey || key === "J") {
        regionOn();
        keepRegion = true;
      }
      move("down");
      key = "(" + Pen.x + "," + Pen.y + ")";
      break;

    case "ArrowUp":
    case "Up":
    case "k":
    case "K":
      if (evt.altKey) {
        evt.preventDefault();
        wallModeOff();
        floor(Pen.x, Pen.y);
      }
      if (evt.shiftKey || key === "K") {
        regionOn();
        keepRegion = true;
      }
      move("up");
      key = "(" + Pen.x + "," + Pen.y + ")";
      break;

    case "ArrowLeft":
    case "Left":
    case "h":
    case "H":
      if (evt.altKey) {
        evt.preventDefault();
        wallModeOff();
        floor(Pen.x, Pen.y);
      }
      if (evt.shiftKey || key === "H") {
        regionOn();
        keepRegion = true;
      }
      move("left");
      key = "(" + Pen.x + "," + Pen.y + ")";
      break;

    case "ArrowRight":
    case "Right":
    case "l":
    case "L":
    case " ":
      if (evt.altKey) {
        evt.preventDefault();
        wallModeOff();
        floor(Pen.x, Pen.y);
      }
      if (evt.shiftKey || key === "L") {
        regionOn();
        keepRegion = true;
      }
      move("right");
      key = "(" + Pen.x + "," + Pen.y + ")";
      break;

    case "1":
    case "2":
    case "3":
    case "4":
    case "5":
    case "6":
    case "7":
    case "8":
    case "9":
    case "0":
      addToLabel(Pen.x, Pen.y, key);
      break;

    case "x":
    case "Backspace":
    case "Del":
      if (Pen.type === "wallMode") {
        if (!removeWall(Pen.x, Pen.y)) {
          wallModeOff();
          deleteFromLabel(Pen.x, Pen.y) ||
            removeFloor(Pen.x, Pen.y) ||
            removeArc(Pen.x, Pen.y);
        }
      } else {
        deleteFromLabel(Pen.x, Pen.y) ||
          removeWall(Pen.x, Pen.y) ||
          removeFloor(Pen.x, Pen.y) ||
          removeArc(Pen.x, Pen.y);
      }
      key = "x"; // record x
      break;

    case "f":
      wallModeOff();
      floor(Pen.x, Pen.y);
      break;

    case "g":
      wallModeOff();
      rock(Pen.x, Pen.y);
      break;

    case "\n":
    case "Enter":
      if (evt.ctrlKey) {
        textImport();
        recording = false; // don't record this one
      } else {
        wallModeOff();
        Pen.x = 0;
        move("down");
        key = "\n"; // record a newline
      }
      break;

    case "d":
      wall(Pen.x, Pen.y, "door");
      break;

    case "w":
      wall(Pen.x, Pen.y, "wall");
      break;

    case "m":
      wallMode(Pen.x, Pen.y);
      break;

    case "p":
      wallModeOff();
      draw(Pen.x, Pen.y, "pillar");
      break;

    case "t":
      wallModeOff();
      draw(Pen.x, Pen.y, "trap");
      break;

    case "b":
      wallModeOff();
      draw(Pen.x, Pen.y, "statue");
      break;

    case "c":
      wallModeOff();
      rotate("floor", Pen.x, Pen.y, "chest");
      break;

    case "s":
      wallModeOff();
      rotate("floor", Pen.x, Pen.y, "stair");
      break;

    case "v":
      variant(Pen.x, Pen.y);
      break;

    case "n":
      wallModeOff();
      rotate("floor", Pen.x, Pen.y, "diagonal");
      break;

    case "a":
      wallModeOff();
      rotate("arcs", Pen.x, Pen.y, "arc");
      break;

    // fschne: Arcs with outer wall
    case "q":
      wallModeOff();
      rotate("arcs", Pen.x, Pen.y, "arcw");
      break;

    case "z":
      if (Map.level < 665) {
        Map.show(Map.level + 1);
      }
      break;

    case "y":
      if (Map.level > 0) {
        Map.show(Map.level - 1);
      }
      break;

    case "W":
      background(Pen.x, Pen.y, "white");
      break;

    case "R":
      background(Pen.x, Pen.y, "red");
      break;

    case "G":
      background(Pen.x, Pen.y, "green");
      break;

    case "B":
      background(Pen.x, Pen.y, "blue");
      break;

    // fschne: Support more colors
    case "Q":
    case "E":
    case "A":
    case "S":
    case "D":
    case "F":
    case "N":
      background(Pen.x, Pen.y, "color" + key);
      break;

    case "Escape":
      if (Map.region.getAttribute("opacity") > 0) {
        Map.region.setAttribute("opacity", 0);
      }
      break;

    case "Z":
      var current = Map.background;
      var next = BACKGROUNDS[current.next];
      Map.setBackground(next);
      break;

    case "X":
      regionCut();
      recording = false; // don't record this one
      break;

    case "C":
      regionCopy();
      recording = false; // don't record this one
      break;

    case "V":
      regionPaste();
      recording = false; // don't record this one
      break;

    case '"':
      showLabelField(Pen.x, Pen.y, null);
      recording = false;
      break;

    case "'":
      showLabelField(Pen.x, Pen.y, "left");
      recording = false;
      break;

    default:
      console.log(
        'Gridmapper does not know how to handle "' +
        key +
        '" (keyIdentifier: ' +
        evt.keyIdentifier +
        ", keyCode: " +
        evt.keyCode +
        ")"
      );
      recording = false;
      break;
  }

  // just pressing shift or ctrl doesn't deactivate the region
  if (!keepRegion && !evt.ctrlKey && !evt.shiftKey) {
    regionOff();
  }

  if (recording) {
    record(key);
  }
}

/**
 * Interprets a sequence of characters as a script: when loading a map
 * or executing the demo instructions. Most of the job will be handled
 * by keyPressed. The interpreter adds a few extra characters which
 * are only useful non-interactively.
 *
 * '.' will reset the Pen's timestamp, simulating a pause (to prevent
 * rotating the last tile)
 *
 * ';' will cause a pause (using a timeout). This is used for the
 * demo. This is why the real code runs as interpretCont – it has a
 * pointer into the code.
 *
 * '(1,2)' will move the pointer to this position.
 *
 * '[0,-1]' will move the pointer by this vector.
 *
 * '"abc"' will insert the label abc (instead of showing an input
 * field). This is why the link in the help area sends a single quote
 * instead of a double quote. The double quote is already taken for
 * strings.
 *
 * 'z' has an alternate implementation: when loading a map, going down
 * should not animate anything, unless we're looking at the demo.
 *
 * '-' moves point to the left (a shorthand for '[-1,0]') and the next
 * character is neither 'v' (a variation) nor a newline.
 *
 * 'f' and 'g' place floor tile and 'advance' to the right.
 *
 * '\01' introduces an edit made by another pen; it is followed by the
 * pen number (Ctrl-A is Start Of Heading), followed by '\02' and the
 * rest of the message (Ctrl-B is Start Of Text).
 *
 * '\02' causes a map reset; it is usually followed by a new map
 * (Ctrl-B is Start Of Text).
 */
function interpretMap(code) {
  interpretCont(code, 0, true, true);
}

/**
 * The same as interpretMap, but for remote commands. That is, floor
 * tiles do not advance, and we are not recording.
 */
function interpretRemote(code) {
  interpretCont(code, 0, false, false);
}

/**
 * The same as interpretMap, but for help keys. That is, floor tiles
 * do not advance, but we are recording.
 */
function interpret(code) {
  interpretCont(code, 0, false, true);
}

/**
 * Continues the interpretation where we left oft. The variables are
 * not initialized so that we can simply pick up where we left off.
 * This is because we want to interrupt the computation using timeouts
 * (when running the demo, for example). 'advance' means we are
 * advancing after floor tiles. 'recording' means we will send it the
 * server when interpreting the code.
 */
function interpretCont(code, i, advance, record) {
  var showLevels = false;
  for (; i < code.length; i++) {
    var direction = null;
    switch (
    code[i] // needs charAt?
    ) {
      case ";":
        setTimeout(function () {
          interpretCont(code, i + 1, advance, record);
        }, 500);
        return; // end!
      case "(":
        var match = code
          .substr(i)
          .match(/^\((\d+(?:\.\d+)?),(\d+(?:\.\d+)?)(?:,(\d+))?\)/);
        if (match) {
          // no longer ints!
          var x = Number(match[1]);
          var y = Number(match[2]);
          var z = Number(match[3]);
          Pen.x = x;
          Pen.y = y;
          if (z >= 0) Map.jump(z);
          // update the pen position if we're not reading from a map
          if (!advance) Pen.update(null);

          i += match[0].length - 1; // will get incremented above
        }
        break;
      case "[":
        var match = code.substr(i).match(/^\[(-?\d+),(-?\d+)\]/);
        if (match) {
          Pen.x += Number(match[1]);
          Pen.y += Number(match[2]);
          i += match[0].length - 1; // will get incremented above
        }
        break;
      case "X":
        var match = code.substr(i).match(/^X\[(\d+),(\d+)\]/);
        if (match) {
          rectangleDelete(
            Pen.x,
            Pen.y,
            Pen.x + Number(match[1]) - 1,
            Pen.y + Number(match[2]) - 1
          );
          i += match[0].length - 1; // will get incremented above
        }
        break;
      case '"':
        var match = code.substr(i).match(/^"(.*?[^\\])"/);
        if (match) {
          setLabel(Pen.x, Pen.y, match[1], false, null); // replace label
          i += match[0].length - 1; // will get incremented above
        }
        break;
      case "'":
        var match = code.substr(i).match(/^'(.*?[^\\])'/);
        if (match) {
          setLabel(Pen.x, Pen.y, match[1], false, "left"); // replace label
          i += match[0].length - 1; // will get incremented above
        }
        break;
      case "-":
        direction = "left";
        break;
      case "z":
        Pen.x = 0;
        Pen.y = 0;
        Pen.type = null;
        Map.jump(Map.level + 1);
        break;
      case "\01":
        // Ctrl-A: interpret the rest of the message using a different
        // Pen.
        var match = code.substr(i).match(/^\01(\d+)\02/);
        if (match) {
          Pen.switchTo(Number(match[1]));
          i += match[0].length - 1; // will get incremented in for loop
        }
        break;
      case "\02": // a simple Ctrl-B means reset the map
        Map.reset();
        advance = true; // change mode for the rest of the code
        showLevels = true; // animate the levels at the end of this message
        break;
      case "\03": // a simple Ctrl-C ends the use of a separate pen
        Pen.switchTo(0);
        break;
      case "f":
      case "g":
        direction = "right";
      // fall through to default
      default:
        keyHandling(code[i], record);
    }

    // advance unless we're going to move elsewhere or switch a
    // variant (peek ahead)
    // fschne: Refactored due to better extendability
    if (advance && direction !== null) {
      // fschne: Support more colors
      var noadvance_next = [
        "v",
        "\n",
        "W",
        "R",
        "G",
        "B",
        "Q",
        "E",
        "A",
        "S",
        "D",
        "F",
        "N"
      ];
      var noadvance_now = ["(", "["];
      if (
        noadvance_now.indexOf(code[i]) < 0 &&
        noadvance_next.indexOf(code[i + 1]) < 0
      ) {
        move(direction);
      }
    }
  }
  if (showLevels) {
    Map.showLevelAnimation();
  }
}

/**
 * Recreates the models based on existing SVG elements (if loading a
 * saved file) and the URL (if loading a page). When loading a file,
 * We go through the elements and add the appropriate data to the
 * various data models. When loading from a URL, we look at the
 * keyword parameter and interpret it as a key sequence one might
 * conceivably type. The only difference is the . character which is
 * used to simulate a little pause (to prevent doors from rotating).
 */
function recreateModel() {
  var re = /^([-a-z0-9]+)_(\d+)_(\d+)$/;
  var z = 0;
  for (var map; (map = document.getElementById("level" + z)); z++) {
    var data = new Tiles();
    recreateModelFor(re, data, document.getElementById("arcs" + z));
    recreateModelFor(re, data, document.getElementById("floor" + z));
    recreateModelFor(re, data, document.getElementById("labels" + z));
    recreateModelForDoors(re, data, document.getElementById("walls" + z));
    Map.levels[z] = data;
  }
  if (z === 0) {
    createLevel(0);
  }
  // start at level 0
  Map.setLevel(0);
  // parse URL
  var str = decodeURIComponent(window.location.search);
  if (str.substr(0, 6) === "?load=") {
    loadMap(wikiUrl + "/" + str.substr(6));
  } else if (str.substr(0, 6) === "?join=") {
    Map.exportarea.value = decodeURIComponent(str.substr(6));
    join();
  } else if (str.charAt(0) === "?") {
    interpretMap(str.substring(1));
  }
  // at the end, move to the top of the dungeon and show it
  Map.jump(0);
  Map.showLevelAnimation();
  moveElements(); // animate grows in the end
}

/**
 * Recreates one particular model based on a matching SVG element.
 * This looks only at id attributes.
 */
function recreateModelFor(re, data, element) {
  for (
    var child = element.firstElementChild;
    child;
    child = child.nextElementSibling
  ) {
    var result = child.getAttribute("id").match(re);
    if (result) {
      child.type = result[1]; // to enable code generation
      var x = result[2];
      var y = result[3];
      if (result[1] === "label") {
        data.get(x, y).label = child;
      } else if (result[1].startsWith("arc")) {
        data.get(x, y).arcs = child;
      } else {
        data.get(x, y).floor = child;
      }
    }
  }
}

/**
 * Recreates one particular wall model on a matching SVG element.
 * This looks only at id attributes.
 */
function recreateModelForDoors(re, data, element) {
  for (
    var child = element.firstElementChild;
    child;
    child = child.nextElementSibling
  ) {
    var result = child.getAttribute("id").match(re);
    if (result) {
      child.type = result[1]; // to enable code generation
      var x = result[2];
      var y = result[3];
      data.get(x, y).walls.push(child);
    }
  }
}

/**
 * Creates the SVG elements for a level. This must match the
 * expectations in recreateModel.
 */
function createLevel(z) {
  var level = document.createElementNS(svgNs, "g");
  level.setAttribute("id", "level" + z);
  level.setAttributeNS(inkscapeNs, "groupmode", "layer");
  level.setAttributeNS(inkscapeNs, "label", "Level " + (z + 1));
  var arcs = document.createElementNS(svgNs, "g");
  arcs.setAttribute("id", "arcs" + z);
  var floor = document.createElementNS(svgNs, "g");
  floor.setAttribute("id", "floor" + z);
  var labels = document.createElementNS(svgNs, "g");
  labels.setAttribute("id", "labels" + z);
  var walls = document.createElementNS(svgNs, "g");
  walls.setAttribute("id", "walls" + z);
  level.appendChild(arcs);
  level.appendChild(floor);
  level.appendChild(labels);
  level.appendChild(walls);
  Map.levelsElement.insertBefore(level, Map.levelsElement.firstChild);
  Map.levels[z] = new Tiles();
}

/**
 * Initialize the user interface.
 */
function initialize() {
  scaleTiles();
  Map.initialize(); // don't reset the Map so we can load from a file
  Pen.update(null); // show it

  // setup the dnd listeners.
  Map.exportarea.addEventListener("dragover", handleDragOver, false);
  Map.exportarea.addEventListener("drop", handleFileSelect, false);

  // Internet Explorer doesn't know how to handle XHTML.
  if (document.documentMode > 0) {
    remove(document.getElementById("label"));
    remove(document.getElementById("textarea"));
    // No text area means no exporting, saving or hosting
    remove(document.getElementById("import-export"));
    remove(document.getElementById("load"));
    remove(document.getElementById("save"));
    remove(document.getElementById("how-to-collaborate"));
    // No XML means no demo
    remove(document.getElementById("demo"));
  }
  recreateModel(); // create model from SVG or URL
}

function remove(element) {
  if (element !== null) {
    if (
      element.nextSibling !== null &&
      element.nextSibling.nodeType === Node.TEXT_NODE
    ) {
      element.parentNode.removeChild(element.nextSibling);
    }
    // do not remove elements because this may cause null pointers elsewhere
    element.setAttribute("display", "none");
  }
}

onload = initialize;
