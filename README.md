# Voidmapper

Voidmapper has been created to draw maps for space ships for my [fan creations](https://www.f-space.de/voidspace/index.html) for [Starslayers](http://www.starslayers.de/).

Voidmapper is based on [Gridmapper](https://alexschroeder.ch/wiki/Gridmapper) by Alex Schroeder.

**As Voidmapper adds new tiles and key bindings the maps are not compatible with Gridmapper!** But Gridmapper maps can be viewed with Voidmapper. Incompatible keys are marked with _voidmapper only_.

## Enhancements and changes

### Tiles and user functionality

- More background colors (`QEASDFN`)
- Arc tiles with walls (`q`)
- Diaginal line (`nvvv`)
- Red and pointy cursor
- SVG download shows all levels
- PNG download in sizes M, L, XL
- `Z` switches background colors only
- Toggle background transparency with `#`
- Background and transparency settings are stored in the text file format, too
- `save` and `join` use the name and password field for a homogeneously user experience.
- Show overall number of levels in the upper right corner near to the current level
- Removed `demo` function
- Rearranged UI

### Background functions

- Load and save maps to www.f-space.de
- Password protection for maps
- Downloaded files named like the map

### Changes in development process

- Javascript, CSS and tile definitions had been extrated to own files in the development process. This causes a much more better support of IDEs while coding.

### Issues

- No support for Microsoft IE and Edge

### Bug fixes

- Retain background color for arc tiles in text format
- Fixed handling of Escape in label fields
- Fixed empty attribute causing SVG parser warning in Firefox

## Basic Usage

Voidmapper is designed to be used by the keyboard. Use the `arrow keys` or `h-j-k-l` to navigate the cursor. Use lower case characters to set [tiles](#Tiles), [walls](#Walls) or [doors](#Walls). Press a key multiple times to rotate the element. Use `v` to choose a variant of an element. Use upper case characters to choose a background color for a tile.

## Key bindings

The following key bindings are supported by Voidmapper.

### UI

    ?       hide/show help section
    Z       switch background color
    #       toggle background transparency

### Navigation

    Arrows  left,right,up,down
    h,H     left
    j,J     down
    k,K     up
    l,L     right
    z       level up
    y       level down
    Enter   new line
    m       toggle wall mode

### Basic modifications

    "       input text, align center
    '       input text, align left
    u       undo last action
    r       redo last action
    x or Backspace    delete content
    Alt     toggle floor (f)

### Tiles

    f   floor
    g   grotto floor
    a   arc floor
    p   pillar
    t   trap
    c   chest
    s   stairs
    n   diagonal
    a   arc floor with wall
    v   choose variant

### Walls

    w   wall
    d   door

### Colors

    W   white
    R   red
    G   green
    B   blue
    Q   grey
    E   dark red
    A   yellow
    S   brown
    D   light blue
    F   dark green
    N   black (voidmapper only)

### Region mode

A region can be marked by holding the shift key pressed and navigate the cursor. A marked region can be copied or cutted and pasted to another position.

    Shift   pressed, mark region
    X       cut the marked region
    C       copy the marked region
    V       paste cutted or copied region
    Escape  unmark region

### Wall mode

The wall mode is a special input mode used to set lots of walls. Enter wall mode with `m`. Navigate as normal and set walls, doors and variants of with `w`, `d` and `v`. Other actions will end the wall mode.

    m   toggle wall mode
    w   wall
    d   door
    v   choose variant

### Unused keys

    e
    i
    o
    T
    U
    I
    O
    P
    Y
    N
    M

## Text file format

Most of your input is recorded and stored in an ordinary text format. This textual representation is used as storage format for the map and can be exported.

### Specials chars in text files and scripts

The text file format knows some special keys and sequences. These are used to navigate the cursor and do separate levels (`z`).

    '       start/end of text
    0-9     number
    (x,y,z) set cursor to x,y on level z
    [dx,dy] move cursor by dx,dy
    -       move curser by -1,0 (=left)
    Space   move cursor by +1,0 (=right)
    h,H     move curser left
    j,J     move curser down
    k,K     move curser up
    l,L     right
    z       level down
    \n      new line
    .       pause simulation
    ;       pause simulation
    \01     CTRL+A, ??
    \02     CTRL+B, reset map

### Example files

    (1,1)fffssss

This file shows a floor with three tiles (`f`) leading to a stair down. The stair was rotated by 180 degrees (`sss` = three times `s`) The floor starts at position 1,1, which is in the upper left corner

    (1,1)fffssssz
    (6,1)dddpvvvd
          c(1,1,0)

Now the end of the stair leads to the next level (`z`). At the end of the stair is some mud (`pvvv` = 4th variant of `p`) and a door (`d`). Left to the mud is a chest (`c`). This is drawn in the new line having six spaces before it. At last the cursor will be set to the start of the floor on level 0.

### Scripting

The text file format supports some special characters and sequences that allows you to script a map. Please take a look at the Gridmapper documentation and demo for further informations for scripting.

## See also

[Gridmapper](https://alexschroeder.ch/wiki/Gridmapper) by Alex Schroeder
